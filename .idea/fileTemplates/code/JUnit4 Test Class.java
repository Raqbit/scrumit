import static org.junit.Assert.*;
import static org.hamcrest.Matchers.*;
#parse("File Header.java")
public class ${NAME} {
  ${BODY}
}