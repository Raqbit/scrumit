# Changelog

## V1.3.0

- Refactored all entities in the UI layer to DTO's.
- Bugfix: Dashboard (Coach, Admin) - Scrum date filter not being set to `today` on initial page load
