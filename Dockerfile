FROM openjdk:8-jre
VOLUME /tmp
ARG JAR_FILE=target/scrumit*.war
COPY ${JAR_FILE} app.war
CMD ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/app.war"]
