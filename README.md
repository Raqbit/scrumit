# ScrumIT

## [Documentation](https://bramceulemans.gitlab.io/scrumit)

## Scrum application for Rijn IJssel AO
[![build status](https://gitlab.com/bramceulemans/scrumit/badges/master/build.svg)](https://gitlab.com/bramceulemans/scrumit/pipelines)
[![coverage report](https://gitlab.com/bramceulemans/scrumit/badges/master/coverage.svg)](https://gitlab.com/bramceulemans/scrumit/pipelines)

### Requirements
- JDK 1.8 and up
- Maven 3.1 and up 
- PostgreSQL 9.6 or up

### Jetbrains IntelliJ Setup
- Install [CheckStyle plugin](https://github.com/jshiell/checkstyle-idea)
- Install [FindBugs plugin](https://plugins.jetbrains.com/plugin/3847-findbugs-idea)

The configuration files for both these plugins are already present in the repository, so click no 
when IntelliJ asks you if you want to overwrite the idea folder. 

### Packaged
Because of the easy deployment nature of jar's, we've chosen to make it our main way of deploying 
ScrumIT. You will need to have `maven` and a version of JDK 1.8 (OpenJDK8, Oracle JDK 1.8, etc.) 
installed.
- Clone the repository
- Create a PostgreSQL user and table called `scrumit`, using the password `scrumit` as well. Or if 
you wish to use a different password you can pass the `-DSPRING_DATASOURCE_PASSWORD=` (or an 
environment variable with the same name)command line argument
- Run in your CLI from within the root of ScrumIT:
  - `$ mvn clean install`
  - `$ mvn package`
  - If everything succeeded you can run `$ mvn spring-boot:run`
  
