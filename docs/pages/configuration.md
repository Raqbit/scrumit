# Configuration

ScrumIT uses Spring Boot, including their configuration management. For documentation on 
configuration of the framework, Tomcat, Hibernate or Spring Security visit: 

- [Externalized Configuration (environment, yaml)](https://docs.spring.io/spring-boot/docs/current/reference/html/boot-features-external-config.html)
- [All Configuration Properties](https://docs.spring.io/spring-boot/docs/current/reference/html/common-application-properties.html)

When using Java properties, the `scrumit.admin.name` format is recommended. However when using 
environment variables, the `SCRUMIT_ADMIN_NAME` format is recommended.

## Admin user

When the application is in production mode, credentials for the admin user must be set. This can be 
done using the following properties:

```properties
scrumit.admin.name=<admin full name>
scrumit.admin.email=<admin email>
scrumit.admin.password=<admin password>
```
    
Example:

```properties
scrumit.admin.name=ScrumIT Admin
scrumit.admin.email=scrumit@rijnijssel.nl
scrumit.admin.password=welkom01
```

## Google Sheets

### Obtaining credentials

In order to obtain access to the Google Sheets integration, credentials to interact with the API 
must be set. To do this;

- Go to [https://console.developers.google.com/apis/dashboard]()
- Create a project for ScrumIT and wait until it's done creating
- Search for "Sheets API" and click "Enable"
- After the API is enabled, go to the Credentials view using the menu on the left.
- Go to the [APIs & Services Credentials](https://console.developers.google.com/apis/credentials) 
  view.
- Click the blue "Create credentials" button and select "Service account key"
- Under "Service account" select "New service account" unless you already have one you'd like to 
  generate a key for, then select that one and skip this.
    - Give the service account a name (i.e. "Production")
    - Select the role "Project" -> "Viewer"
- Click the blue "Create" button and save the service account key to a safe location.

### Configuring API connection

#### Option 1 (Service account key file)

Save or copy the file to a folder on the server and set the following property:

```properties
scrumit.sheets.service-account=<path to service account file>
```
    
Example:

```properties
scrumit.sheets.service-account=/home/scrumit/scrumit-f14781984109.json

```

This will then load the file, read the contents using the UTF-8 charset and connect to the Google 
Sheets API.

#### Option 2 (Service account key json content)

Open the service account json file, copy all the contents and put them in the property:

```properties
scrumit.sheets.service-account='{ ... service account here }'
```

This is a good option when you don't have access to the file system, for example on Google App 
Engine or Heroku.

## Database
