# ScrumIT

This documentation will explain how to contribute to extend, configure and deploy ScrumIT. 

## Commands

* `mkdocs new [dir-name]` - Create a new project.
* `mkdocs serve` - Start the live-reloading docs server.
* `mkdocs build` - Build the documentation site.
* `mkdocs help` - Print this help message.

## Project layout

    src/
        main/         # Application source code
        test/         # Application test code
    docs/
        mkdocs.yml    # The configuration file.
        pages/        # Documentation pages
            index.md  # Homepage
            ...       # Other docs pages, images and other files.
    .gitlab-ci.yml    # GitLab CI/CD file
    Dockerfile        # Dockerfile based on OpenJDK 8
