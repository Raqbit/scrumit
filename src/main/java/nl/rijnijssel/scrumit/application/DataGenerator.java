/*
 * Scrumit
 * Copyright (C) 2019  Bram Ceulemans (bramceulemans@me.com)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

package nl.rijnijssel.scrumit.application;

import com.devskiller.jfairy.Fairy;
import com.devskiller.jfairy.producer.BaseProducer;
import com.devskiller.jfairy.producer.person.Person;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import lombok.extern.slf4j.Slf4j;
import nl.rijnijssel.scrumit.config.ScrumitConfiguration;
import nl.rijnijssel.scrumit.domain.Role;
import nl.rijnijssel.scrumit.domain.entity.AnswerEntity;
import nl.rijnijssel.scrumit.domain.entity.CoachEntity;
import nl.rijnijssel.scrumit.domain.entity.StudentEntity;
import nl.rijnijssel.scrumit.domain.entity.UserEntity;
import nl.rijnijssel.scrumit.domain.repository.AnswerRepository;
import nl.rijnijssel.scrumit.domain.repository.CoachRepository;
import nl.rijnijssel.scrumit.domain.repository.StudentRepository;
import nl.rijnijssel.scrumit.domain.repository.UserRepository;
import nl.rijnijssel.scrumit.domain.service.CrudService;
import nl.rijnijssel.scrumit.domain.service.UserService;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Profile;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class DataGenerator {

    private final ScrumitConfiguration config;

    @Autowired
    public DataGenerator(ScrumitConfiguration config) {
        this.config = config;
    }

    /**
     * Generate the default admin user for production environment.
     *
     * @return the command line runner for generating an admin user.
     */
    @Bean
    @Profile({"production", "local"})
    public CommandLineRunner generateDefaultAdmin(UserService userService,
        PasswordEncoder passwordEncoder) {
        String password = config.getAdmin().getPassword();
        if (StringUtils.isBlank(password) || password.length() < 8) {
            throw new IllegalArgumentException(
                "Admin password has to be set and needs to have at least 8 characters.");
        }
        return args -> {
            if (userService.findByEmail(config.getAdmin().getEmail()) != null) {
                return;
            }
            UserEntity entity = new UserEntity(config.getAdmin().getEmail(),
                config.getAdmin().getName(),
                passwordEncoder.encode(password), Role.ADMIN);
            userService.getRepository().save(entity);
            log.info("Saved admin user.");
        };
    }

    /**
     * Load test data into the application.
     *
     * @return the application runner for generating development data.
     */
    @Bean
    @Profile({"local"})
    public ApplicationRunner generateDevelopmentData(UserRepository userRepository,
        CoachRepository coachRepository, StudentRepository studentRepository,
        AnswerRepository answerRepository) {
        return args -> {
            if (hasData(userRepository) || hasData(coachRepository) || hasData(studentRepository)
                || hasData(answerRepository)) {
                return;
            }

            log.info("Generating development data.");
            Fairy fairy = Fairy.create(Locale.ENGLISH);

            /* Generate users for students */
            log.info("- Generating student users.");
            List<UserEntity> studentUsers = new ArrayList<>();
            for (int i = 0; i < 40; i++) {
                Person person = fairy.person();
                UserEntity user = new UserEntity(person.getEmail(), person.getFullName(),
                    "Welkom01",
                    Role.STUDENT);
                studentUsers.add(user);
            }
            studentUsers = userRepository.saveAll(studentUsers);
            log.info("- Generated {} student users.", studentUsers.size());

            /* Generate users for coaches */
            log.info("- Generating coach users.");
            List<UserEntity> coachUsers = new ArrayList<>();
            for (int i = 0; i < 5; i++) {
                Person person = fairy.person();
                UserEntity user = new UserEntity(person.getEmail(), person.getFullName(),
                    "Welkom01",
                    Role.COACH);
                coachUsers.add(user);
            }
            coachUsers = userRepository.saveAll(studentUsers);
            log.info("- Generated {} coach users.", coachUsers.size());

            /* Generate coaches */
            log.info("- Generating coaches.");
            List<CoachEntity> coaches = new ArrayList<>();
            for (UserEntity user : coachUsers) {
                Person person = fairy.person();
                String abbreviation = fairy.baseProducer().letterify("?????");
                CoachEntity coach = new CoachEntity();
                coach.setAbbreviation(abbreviation);
                coach.setUser(user);
                coaches.add(coach);
            }
            coaches = coachRepository.saveAll(coaches);
            log.info("- Generated {} coaches.", coaches.size());

            /* Generate students */
            log.info("- Generating students.");
            List<StudentEntity> students = new ArrayList<>();
            for (UserEntity user : studentUsers) {
                BaseProducer p = fairy.baseProducer();
                String sid = p.numerify("00######");
                String phase = p
                    .randomElement("Orientatie", "Development", "Expert");
                String clazz = "ICAE" + p.randomElement("D", "E", "F");
                int year = p.randomBetween(1, 3);
                StudentEntity student = new StudentEntity(sid, user, phase, clazz, year,
                    p.randomElement(coaches));
                student.setProjectName(
                    p.randomElement("OBT1 - NAO", "DBT1 - BookOnShelf", "OBT1 - @YourService",
                        "DBT1 - Smoothboard Stylers"));
                student.setTable(p.numerify("#", 1, 6));
                students.add(student);
            }
            students = studentRepository.saveAll(students);
            log.info("- Generated {} students.", students.size());

            /* Generate answers */
            log.info("- Generating answers.");
            List<AnswerEntity> answers = new ArrayList<>();
            BaseProducer p = fairy.baseProducer();
            for (StudentEntity user : students) {
                for (int i = 0; i < 20; i++) {
                    AnswerEntity answer = new AnswerEntity();
                    answer.setYesterday(fairy.textProducer().sentence());
                    answer.setProblem(fairy.textProducer().sentence());
                    answer.setToday(fairy.textProducer().sentence());
                    answer.setSupport(fairy.textProducer().sentence());
                    answer.setOwner(user);
                    Date date = new Date();
                    answer.setCreatedAt(
                        DateUtils.addDays(date, p.randomElement(Arrays.asList(-2, -1, 0, 0))));
                    answers.add(answer);
                }
            }
            answers = answerRepository.saveAll(answers);
            log.info("- Generated {} answers.", answers.size());
            log.info("- Done generating development data.", answers.size());
        };
    }

    private <T extends CrudService> boolean hasData(T service) {
        return service.getRepository().count() != 0L;
    }

    private <T extends JpaRepository> boolean hasData(T repo) {
        return repo.count() != 0L;
    }
}
