/*
 * Scrumit
 * Copyright (C) 2019  Bram Ceulemans (bramceulemans@me.com)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

package nl.rijnijssel.scrumit.application;

import com.vaadin.server.VaadinSession;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import javax.annotation.PostConstruct;
import nl.rijnijssel.scrumit.config.ScrumitConfiguration;
import nl.rijnijssel.scrumit.domain.dto.UserDTO;
import nl.rijnijssel.scrumit.web.security.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.NoSuchMessageException;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Component;

@Component
public class I18n {

    private static MessageSourceAccessor messageSourceAccessor;
    private static Locale defaultLocale;
    private static List<Locale> supportedLocales;
    private final MessageSource messageSource;
    private final ScrumitConfiguration config;

    /**
     * Dependency injection constructor for the I18n class.
     *
     * @param messageSource the message source
     */
    @Autowired
    public I18n(MessageSource messageSource, ScrumitConfiguration config) {
        this.messageSource = messageSource;
        this.config = config;
    }

    /**
     * Find a message by it's key, using the current UI locale.
     *
     * @param key the key to search for
     * @return the message string
     */
    public static String get(String key) {
        VaadinSession session = VaadinSession.getCurrent();
        Locale locale = session != null ? session.getLocale() : null;

        if (messageSourceAccessor == null) {
            return "?" + key + "?";
        }
        return get(key, locale);
    }

    /**
     * Find a message by it's key and locale.
     *
     * @param key the key to search for
     * @param locale the locale to search for
     * @return the message string for the given locale
     */
    public static String get(String key, Locale locale) {
        try {
            return messageSourceAccessor.getMessage(key, locale);
        } catch (NoSuchMessageException e) {
            String defaultMessage = "?" + key + "?";
            return messageSourceAccessor.getMessage(key, defaultMessage, Locale.ENGLISH);
        }
    }

    /**
     * Get the locale for the current user, if null, use default application locale.
     *
     * @return the locale for the current user
     */
    public static Locale getCurrentLocale() {
        UserDTO currentUser = SecurityUtils.getCurrentUser();
        if (currentUser == null || currentUser.getLanguage() == null) {
            return defaultLocale;
        } else {
            return currentUser.getLanguage();
        }
    }

    /**
     * Get all locales that are supported by ScrumIT.
     *
     * @return the locales
     */
    public static List<Locale> getSupportedLocales() {
        if (supportedLocales == null || supportedLocales.isEmpty()) {
            supportedLocales = new ArrayList<>();
            supportedLocales.add(Locale.forLanguageTag("en"));
            supportedLocales.add(Locale.forLanguageTag("nl"));
            supportedLocales.add(Locale.forLanguageTag("es"));
            return supportedLocales;
        }
        return supportedLocales;
    }

    public static String error() {
        return get("notification.error");
    }

    /**
     * Post constructor for I18n class.
     */
    @PostConstruct
    private void init() {
        messageSourceAccessor = new MessageSourceAccessor(messageSource, defaultLocale);
        setDefaultLocale(config.getDefaultLocale());
    }

    /**
     * Set the default locale for this application, defaults to the <code>scrumit.locale</code>
     * property.
     *
     * @param defaultLocale the default locale
     */
    private void setDefaultLocale(Locale defaultLocale) {
        I18n.defaultLocale = defaultLocale;
    }
}
