/*
 * Scrumit
 * Copyright (C) 2018  Bram Ceulemans (bramceulemans@me.com)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

package nl.rijnijssel.scrumit.application;

import java.lang.reflect.Type;
import java.util.List;
import nl.rijnijssel.scrumit.domain.dto.ActionDTO;
import nl.rijnijssel.scrumit.domain.dto.AnswerDTO;
import nl.rijnijssel.scrumit.domain.dto.CoachDTO;
import nl.rijnijssel.scrumit.domain.dto.SettingDTO;
import nl.rijnijssel.scrumit.domain.dto.StudentDTO;
import nl.rijnijssel.scrumit.domain.dto.UserDTO;
import org.modelmapper.TypeToken;

public class MapTypes {

    public static final Type USER_DTO_LIST = new TypeToken<List<UserDTO>>() {
    }.getType();
    public static final Type ACTION_DTO_LIST = new TypeToken<List<ActionDTO>>() {
    }.getType();
    public static final Type COACH_DTO_LIST = new TypeToken<List<CoachDTO>>() {
    }.getType();
    public static final Type ANSWER_DTO_LIST = new TypeToken<List<AnswerDTO>>() {
    }.getType();
    public static final Type SETTING_DTO_LIST = new TypeToken<List<SettingDTO>>() {
    }.getType();
    public static final Type STUDENT_DTO_LIST = new TypeToken<List<StudentDTO>>() {
    }.getType();
}
