/*
 * Scrumit
 * Copyright (C) 2018  Bram Ceulemans (bramceulemans@me.com)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

package nl.rijnijssel.scrumit.application;

/**
 * Class that stores several constants for ScrumIT.
 */
public class ScrumitConstants {

    /* Date formats */
    public static final String DATE_FORMAT_LONG = "%1$tB %1$te, %1$tY, %1$tR";

    /* Static URL's */
    public static final String APP_URL = "/app";
    public static final String LOGIN_URL = "/sign-in";
    public static final String LOGOUT_URL = "/sign-out";
    public static final String LOGIN_FAILURE_URL = "/sign-in?failed";
}
