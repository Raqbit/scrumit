/*
 * Scrumit
 * Copyright (C) 2018  Bram Ceulemans (bramceulemans@me.com)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

package nl.rijnijssel.scrumit.application;

import nl.rijnijssel.scrumit.domain.ActionType;
import nl.rijnijssel.scrumit.domain.dto.ActionDTO;
import nl.rijnijssel.scrumit.domain.dto.CoachDTO;
import nl.rijnijssel.scrumit.domain.dto.UserDTO;
import nl.rijnijssel.scrumit.domain.service.ActionService;
import nl.rijnijssel.scrumit.domain.service.SettingService;
import nl.rijnijssel.scrumit.domain.service.UserService;
import nl.rijnijssel.scrumit.event.StudentAnsweredEvent;
import nl.rijnijssel.scrumit.event.UserLoginEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.stereotype.Component;


@EnableAsync(proxyTargetClass = true)
@Async
@Component
public class ScrumitEventHandler {

    private final SettingService settingService;
    private final ActionService actionService;
    private final UserService userService;

    /**
     * Scrumit event handler constructor.
     */
    @Autowired
    public ScrumitEventHandler(SettingService settingService, ActionService actionService,
        UserService userService) {
        this.settingService = settingService;
        this.actionService = actionService;
        this.userService = userService;
    }

    @EventListener(ApplicationReadyEvent.class)
    public void createSettings() {
        settingService.createMissingSettings();
    }

    /**
     * Event handler for user login, runs asynchronously.
     *
     * @param event the user login event
     */
    @EventListener(UserLoginEvent.class)
    public void onUserLoginEvent(UserLoginEvent event) {
        UserDTO user = event.getUser();
        /* Increase the login count for a user */
        long loginCount = user.getLoginCount();
        user.setLoginCount(++loginCount);
        user = userService.save(user);
        /* Save the login action */
        ActionDTO action = new ActionDTO(ActionType.LOGIN, user.getEmail(), user);
        actionService.save(action);
    }

    /**
     * Event handler for scrum answers, runs asynchronously.
     *
     * @param event the answered event
     */
    @EventListener(StudentAnsweredEvent.class)
    public void onStudentAnsweredEvent(StudentAnsweredEvent event) {
        UserDTO user = event.getStudent().getUser();
        CoachDTO coach = event.getAnswer().getSupportCoach();
        String details = coach != null ? coach.getAbbreviation() : "";
        ActionDTO action = new ActionDTO(ActionType.ANSWER, details, user);
        actionService.save(action);
    }
}
