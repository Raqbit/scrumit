/*
 * Scrumit
 * Copyright (C) 2018  Bram Ceulemans (bramceulemans@me.com)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

package nl.rijnijssel.scrumit.application;

import lombok.extern.slf4j.Slf4j;
import nl.rijnijssel.scrumit.application.integration.sheets.SheetsIntegration;
import nl.rijnijssel.scrumit.application.utils.StudentUtils;
import nl.rijnijssel.scrumit.domain.SettingName;
import nl.rijnijssel.scrumit.domain.service.SettingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class ScrumitScheduler {

    private final SheetsIntegration sheets;
    private final SettingService settingService;

    /**
     * ScrumitScheduler constructor for dependency injection.
     */
    @Autowired
    public ScrumitScheduler(SheetsIntegration sheetsIntegration, SettingService settingService) {
        this.sheets = sheetsIntegration;
        this.settingService = settingService;
    }

    /**
     * A job to synchronize all students every 12 hours.
     */
    @Scheduled(initialDelay = 1000L, fixedRate = 720000L)
    public void synchronizeStudents() {
        if (settingService.getBooleanSetting(SettingName.SPREADSHEETS_ENABLED)) {
            String spreadsheet = settingService.getStringSetting(SettingName.SPREADSHEET_ID);
            StudentUtils.synchronize(sheets, spreadsheet);
        } else {
            log.debug("Spreadsheets integration is disabled, not synchronized.");
        }
    }
}
