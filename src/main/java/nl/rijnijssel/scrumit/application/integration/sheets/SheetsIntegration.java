/*
 * Scrumit
 * Copyright (C) 2018  Bram Ceulemans (bramceulemans@me.com)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

package nl.rijnijssel.scrumit.application.integration.sheets;

import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.sheets.v4.Sheets;
import com.google.api.services.sheets.v4.SheetsScopes;
import com.google.api.services.sheets.v4.model.SpreadsheetProperties;
import com.google.common.base.Charsets;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.security.GeneralSecurityException;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import javax.annotation.PostConstruct;
import lombok.extern.slf4j.Slf4j;
import nl.rijnijssel.scrumit.application.integration.sheets.model.StudentBean;
import nl.rijnijssel.scrumit.application.integration.sheets.sheet.StudentSheet;
import nl.rijnijssel.scrumit.application.utils.StudentUtils;
import nl.rijnijssel.scrumit.config.ScrumitConfiguration;
import nl.rijnijssel.scrumit.domain.Role;
import nl.rijnijssel.scrumit.domain.SettingName;
import nl.rijnijssel.scrumit.domain.entity.CoachEntity;
import nl.rijnijssel.scrumit.domain.entity.StudentEntity;
import nl.rijnijssel.scrumit.domain.entity.UserEntity;
import nl.rijnijssel.scrumit.domain.service.CoachService;
import nl.rijnijssel.scrumit.domain.service.SettingService;
import nl.rijnijssel.scrumit.domain.service.StudentService;
import nl.rijnijssel.scrumit.domain.service.UserService;
import org.apache.commons.lang3.StringUtils;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.context.annotation.ApplicationScope;

@Service
@ApplicationScope
@Slf4j
public class SheetsIntegration {

    private final UserService userService;
    private final CoachService coachService;
    private final StudentService studentService;
    private final PasswordEncoder passwordEncoder;
    private final SettingService settingService;
    private final ModelMapper mapper;

    private ScrumitConfiguration config;
    private StudentSheet studentSheet;
    private Sheets sheets;
    private String defaultDomain;
    private String defaultStudentPassword;
    private List<String> studentIds;
    private boolean initialized = false;

    /**
     * GoogleSheets integration constructor, set needed variables here.
     */
    @Autowired
    public SheetsIntegration(UserService userService, CoachService coachService,
        StudentService studentService, PasswordEncoder passwordEncoder,
        SettingService settingService, ScrumitConfiguration config, ModelMapper mapper) {
        this.userService = userService;
        this.coachService = coachService;
        this.studentService = studentService;
        this.passwordEncoder = passwordEncoder;
        this.settingService = settingService;
        this.config = config;
        this.mapper = mapper;
    }

    /**
     * Initialize the Google Sheets service.
     */
    @PostConstruct
    public void init() {
        Collection<String> scopes = Collections.singletonList(SheetsScopes.SPREADSHEETS_READONLY);
        try {
            HttpTransport httpTransport = GoogleNetHttpTransport.newTrustedTransport();
            JsonFactory jsonFactory = JacksonFactory.getDefaultInstance();
            InputStream configStream = getConfigStream();
            GoogleCredential serviceAccount = GoogleCredential.fromStream(configStream)
                .createScoped(scopes);

            sheets = new Sheets.Builder(httpTransport, jsonFactory, serviceAccount)
                .setApplicationName("ScrumIT")
                .build();
            log.info("Google Sheets API: Successfully connected with ScrumIT");
            this.studentSheet = new StudentSheet(config);
            this.initialized = true;
        } catch (GeneralSecurityException | IOException | NullPointerException e) {
            log.error("Google Sheets API: Config was not found or parsed.",
                e.getLocalizedMessage());
        }
    }

    /**
     * Get the input stream from either the environment variables or a file on the system.
     *
     * @return the input stream
     */
    private InputStream getConfigStream() throws FileNotFoundException {
        String homeDirectory = System.getProperty("user.home");
        String[] paths = new String[]{
            config.getSheets().getServiceAccount(),
            "." + config.getSheets().getServiceAccount(),
            homeDirectory + "/" + config.getSheets().getServiceAccount(),
            homeDirectory + "/." + config.getSheets().getServiceAccount(),
            homeDirectory + "/Documents/" + config.getSheets().getServiceAccount()
        };

        File configFile;
        for (String path : paths) {
            configFile = new File(path);
            if (configFile.exists()) {
                String absolutePath = configFile.getAbsolutePath();
                log.info("Google Sheets API: Using config file: {}", absolutePath);
                return new FileInputStream(configFile);
            }
        }

        log.info("Google Sheets API: Using JSON string as config");
        return new ByteArrayInputStream(
            config.getSheets().getServiceAccount().getBytes(Charsets.UTF_8));
    }

    /**
     * Get the service instance for this application.
     *
     * @return sheets service
     * @throws SheetsIntegrationException when not initialized
     */
    public Sheets getService() {
        if (!initialized) {
            throw new SheetsIntegrationException("Sheets was not initialized.");
        }
        return sheets;
    }

    /**
     * Get a list of a list of objects from the Google API. You probably want to convert this into a
     * {@link StudentSheet}.
     *
     * @param spreadsheetId spreadsheet ID to get
     * @return list of spreadsheet rows
     * @throws IOException when the gsheet cannot be found or access is denied
     */
    private List<List<Object>> getSheet(String spreadsheetId) throws IOException {
        return sheets
            .spreadsheets()
            .values()
            .get(spreadsheetId, "A1:ZZ")
            .execute()
            .getValues();
    }

    /**
     * Get a student sheet object.
     *
     * @param spreadsheetId get the student sheet
     * @return student sheet object
     * @throws SheetsIntegrationException when not initialized
     */
    public StudentSheet getStudentSheet(String spreadsheetId) throws SheetsIntegrationException {
        if (!initialized) {
            throw new SheetsIntegrationException("Sheets was not initialized.");
        }
        try {
            return studentSheet.convert(getSheet(spreadsheetId));
        } catch (IOException e) {
            log.error("Could not find spreadsheet. {}", e);
            return studentSheet;
        }
    }

    /**
     * Check whether a spreadsheet exists.
     *
     * @param spreadsheetId spreadsheet ID to check
     * @return if the sheet exists
     * @throws SheetsIntegrationException when not initialized
     */
    public boolean sheetExists(String spreadsheetId) throws SheetsIntegrationException {
        if (!initialized) {
            throw new SheetsIntegrationException("Sheets was not initialized.");
        }
        try {
            SpreadsheetProperties properties = sheets.spreadsheets().get(spreadsheetId)
                .execute()
                .getProperties();
            boolean exists = StringUtils.isNotEmpty(properties.getTitle());
            log.debug("inside sheetExists(): Spreadsheet: {}, {}", exists, spreadsheetId);
            return exists;
        } catch (IOException e) {
            log.debug("Spreadsheet does not exist.", e);
            return false;
        }
    }


    /**
     * Import a list of students to the database.
     *
     * @param studentSheet the student sheet to import
     * @throws SheetsIntegrationException when not initialized
     */
    @Async
    public void importStudents(StudentSheet studentSheet) throws SheetsIntegrationException {
        if (!initialized) {
            throw new SheetsIntegrationException("Sheets was not initialized.");
        }
        importStudents(studentSheet.getStudents());
    }

    /**
     * Import a list of students to the database.
     *
     * @param students students to import
     */
    private void importStudents(List<StudentBean> students) {
        studentIds = studentService.getRepository().findAllStudentIds();
        defaultDomain = settingService.getStringSetting(SettingName.STUDENT_DEFAULT_EMAIL_DOMAIN);
        defaultStudentPassword = settingService
            .getStringSetting(SettingName.STUDENT_DEFAULT_PASSWORD);
        students.forEach(this::doImport);
    }

    /**
     * Import logic for a single student.
     *
     * @param bean student bean to import
     */
    private void doImport(StudentBean bean) {
        StudentEntity student = studentService.getRepository()
            .findStudentBySid(bean.getSid());

        if (student == null) {
            String coachAbbreviation = bean.getCoach();
            student = mapper.map(bean, StudentEntity.class);

            CoachEntity coach = coachService.getRepository().findByAbbreviation(coachAbbreviation);
            if (coach == null) {
                coach = new CoachEntity(coachAbbreviation, null);
                coach = coachService.getRepository().save(coach);
            }
            student.setCoach(coach);

            String email = StudentUtils.getEmailForStudent(student, defaultDomain);
            UserEntity user = userService.getRepository().findByEmail(email);

            if (user == null) {
                String name = bean.getFullName();
                user = new UserEntity(email, name, passwordEncoder.encode(defaultStudentPassword),
                    Role.STUDENT);
                user = userService.getRepository().save(user);
            }

            student.setUser(user);
            student.setTable(bean.getTable());
            student.setTableThursday(bean.getTableThursday());
            student.setProjectName(bean.getProject());
            student.setProjectLevel(bean.getProjectLevel());
            student.setProjectTech(bean.getProjectTech());
            student.setProjectClient(bean.getProjectClient());
            student.setProjectSupervisor(bean.getProjectSupervisor());
        } else {
            UserEntity user = student.getUser();
            if (user != null) {
                user.setName(bean.getFullName());
            }
            String coachAbbreviation = bean.getCoach();
            CoachEntity coach = coachService.getRepository().findByAbbreviation(coachAbbreviation);
            student.setCoach(coach);
            student.setClazz(bean.getClazz());
            student.setYear(bean.getYear());
            student.setStatus(bean.getStatus());
        }

        if (studentIds != null && !studentIds.contains(student.getSid())) {
            UserEntity user = student.getUser();
            if (user != null) {
                user.setDisabled(true);
            }
        }

        student = studentService.getRepository().save(student);
        log.info("Student saved: {} {}", student.getSid(), student.getUser().getName());
    }
}
