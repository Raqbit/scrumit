/*
 * Scrumit
 * Copyright (C) 2018  Bram Ceulemans (bramceulemans@me.com)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

package nl.rijnijssel.scrumit.application.integration.sheets;

import java.util.List;
import java.util.Map;
import lombok.experimental.UtilityClass;
import nl.rijnijssel.scrumit.application.integration.sheets.model.StudentBean;
import nl.rijnijssel.scrumit.application.integration.sheets.sheet.StudentSheet.Header;

@UtilityClass
public class SheetsMapper {

    /**
     * Map a list of objects to an actual student bean.
     *
     * @param list map a list of objects to a student bean
     * @return student bean
     */
    public static StudentBean mapToStudentBean(Map<Header, Integer> headers, List<Object> list) {
        StudentBean studentBean = new StudentBean();
        headers.forEach((header, index) -> {
            if (index > list.size() - 1) {
                return;
            }
            String value = (String) list.get(index);
            mapStudentVariable(studentBean, header, value);
        });
        return studentBean;
    }

    /**
     * Map the header to a student bean.
     *
     * @param bean the bean to use for mapping
     * @param header the header variable to set
     * @param value the value to set to the field
     */
    private static void mapStudentVariable(StudentBean bean, Header header, String value) {
        if (header == Header.STUDENT_NUMBER) {
            bean.setSid(value);
        } else if (header == Header.FIRST_NAME) {
            bean.setFirstName(value);
        } else if (header == Header.PREPOSITION) {
            bean.setPreposition(value);
        } else if (header == Header.LAST_NAME) {
            bean.setLastName(value);
        } else if (header == Header.NAME) {
            bean.setName(value);
        } else if (header == Header.CLASS) {
            bean.setClazz(value);
        } else if (header == Header.STATUS) {
            bean.setStatus(value);
        } else if (header == Header.YEAR) {
            bean.setYear(Integer.parseInt(value));
        } else if (header == Header.COACH) {
            bean.setCoach(value);
        } else if (header == Header.TABLE) {
            bean.setTable(value);
        } else if (header == Header.TABLE_THURSDAY) {
            bean.setTableThursday(value);
        } else if (header == Header.PROJECT) {
            bean.setProject(value);
        } else if (header == Header.PROJECT_LEVEL) {
            bean.setProjectLevel(value);
        } else if (header == Header.PROJECT_TECH) {
            bean.setProjectTech(value);
        } else if (header == Header.PROJECT_CLIENT) {
            bean.setProjectClient(value);
        } else if (header == Header.PROJECT_SUPERVISOR) {
            bean.setProjectSupervisor(value);
        }
    }

}
