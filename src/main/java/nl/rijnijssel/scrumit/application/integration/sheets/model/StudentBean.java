/*
 * Scrumit
 * Copyright (C) 2018  Bram Ceulemans (bramceulemans@me.com)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

package nl.rijnijssel.scrumit.application.integration.sheets.model;

import java.util.function.Predicate;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;

@Getter
@Setter
public class StudentBean {

    private String sid;
    private String firstName;
    private String preposition;
    private String lastName;
    private String name;
    private String clazz;
    private String status;
    private String coach;
    private String table;
    private String tableThursday;
    private String project;
    private String projectLevel;
    private String projectTech;
    private String projectClient;
    private String projectSupervisor;
    private int year;

    /**
     * Get the full name of a student.
     *
     * @return the full name
     */
    public String getFullName() {
        Predicate<String> empty = StringUtils::isEmpty;
        if (!empty.test(firstName) && empty.test(preposition) && !empty.test(lastName)) {
            return String.format("%s %s", firstName, lastName);
        } else if (!empty.test(firstName) && !empty.test(preposition) && !empty.test(lastName)) {
            return String.format("%s %s %s", firstName, preposition, lastName);
        } else {
            return name;
        }
    }

    @Override
    public String toString() {
        return String.format("%s: %s", this.getSid(), this.getName());
    }
}
