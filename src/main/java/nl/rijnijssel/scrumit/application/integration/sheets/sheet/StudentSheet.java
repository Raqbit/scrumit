/*
 * Scrumit
 * Copyright (C) 2018  Bram Ceulemans (bramceulemans@me.com)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

package nl.rijnijssel.scrumit.application.integration.sheets.sheet;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import javax.validation.Valid;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import nl.rijnijssel.scrumit.application.integration.sheets.SheetsMapper;
import nl.rijnijssel.scrumit.application.integration.sheets.model.StudentBean;
import nl.rijnijssel.scrumit.config.ScrumitConfiguration;
import nl.rijnijssel.scrumit.config.nested.SheetsConfiguration;
import org.apache.commons.lang3.StringUtils;

@Slf4j
public class StudentSheet {

    private List<List<Object>> rawData;
    private Map<Header, Integer> headers;
    @Getter
    private List<StudentBean> students;
    private ScrumitConfiguration config;

    public StudentSheet(ScrumitConfiguration config) {
        this.config = config;
        this.rawData = new ArrayList<>();
    }

    /**
     * Converts the raw data into an actual list of {@link StudentBean}.
     *
     * @return the converted student sheet which can be used.
     */
    public StudentSheet convert(List<List<Object>> listOfRows) {
        rawData = new ArrayList<>(listOfRows);

        if (rawData.size() > 1) {
            headers = getHeaders(rawData);
            students = getStudents(rawData);
        }

        return this;
    }

    /**
     * Get the headers of the Student table.
     *
     * @param sheet gsheet to extract headers from
     * @return list of headers, together with their column index
     */
    private Map<Header, Integer> getHeaders(List<List<Object>> sheet) {
        Map<Header, Integer> localHeaders = new TreeMap<>();
        List<Object> columns = sheet.get(0);

        /* Get the columns and their indexes */
        for (int index = 0; index < columns.size(); index++) {
            Object column = columns.get(index);
            String c = String.valueOf(column);

            for (Header header : Header.values()) {
                @Valid SheetsConfiguration sheets = config.getSheets();
                if (StringUtils.equalsAnyIgnoreCase(sheets.getColumnMapping().get(header), c)) {
                    localHeaders.put(header, index);
                }
            }
        }
        /* Remove the header row */
        this.rawData.remove(0);
        log.debug("Found columns: {}", localHeaders);
        return localHeaders;
    }

    /**
     * Get all student beans from a list of a list of objects.
     *
     * @param sheet the sheet, as a list of object rows
     * @return a list of student beans, mapped to all of the rows.
     */
    private List<StudentBean> getStudents(List<List<Object>> sheet) {
        List<StudentBean> studentBeans = new ArrayList<>();
        /* Amount of students */
        int studentCount = sheet.size();
        log.info("Found {} students", studentCount);
        for (List<Object> row : sheet) {
            StudentBean student = SheetsMapper.mapToStudentBean(headers, row);
            studentBeans.add(student);
        }
        return studentBeans;
    }

    /**
     * Enum for all the headers that are in the Google Sheet.
     */
    public enum Header {
        STUDENT_NUMBER,
        FIRST_NAME,
        PREPOSITION,
        LAST_NAME,
        NAME,
        CLASS,
        STATUS,
        YEAR,
        COACH,
        TABLE,
        TABLE_THURSDAY,
        PROJECT,
        PROJECT_LEVEL,
        PROJECT_TECH,
        PROJECT_CLIENT,
        PROJECT_SUPERVISOR
    }
}
