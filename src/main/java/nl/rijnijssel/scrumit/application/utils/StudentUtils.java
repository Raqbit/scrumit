/*
 * Scrumit
 * Copyright (C) 2018  Bram Ceulemans (bramceulemans@me.com)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

package nl.rijnijssel.scrumit.application.utils;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;
import nl.rijnijssel.scrumit.application.integration.sheets.SheetsIntegration;
import nl.rijnijssel.scrumit.application.integration.sheets.model.StudentBean;
import nl.rijnijssel.scrumit.application.integration.sheets.sheet.StudentSheet;
import nl.rijnijssel.scrumit.domain.entity.StudentEntity;

@Slf4j
@UtilityClass
public class StudentUtils {


    /**
     * Synchronize all students with a given spreadsheet.
     *
     * @param sheetsIntegration sheets integration to use for the import
     * @param spreadsheetId spreadsheet id for the spreadsheet to import
     * @return list of mapped student beans.
     */
    public static List<StudentBean> synchronize(
        @Nullable SheetsIntegration sheetsIntegration,
        @Nonnull String spreadsheetId) {
        log.info("Synchronizing students.");
        if (sheetsIntegration != null) {
            StudentSheet studentSheet = sheetsIntegration.getStudentSheet(spreadsheetId);
            if (studentSheet != null) {
                sheetsIntegration.importStudents(studentSheet);
                log.info("Synchronized students. ({})", spreadsheetId);
                return studentSheet.getStudents();
            }
        }
        log.info("Error synchronizing students.");
        return new ArrayList<>();
    }

    public static String getEmailForStudent(@Nonnull StudentEntity student, String domain) {
        return student.getSid() + "@" + domain;
    }

}
