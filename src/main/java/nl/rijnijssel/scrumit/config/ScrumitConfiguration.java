/*
 * Scrumit
 * Copyright (C) 2019  Bram Ceulemans (bramceulemans@me.com)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

package nl.rijnijssel.scrumit.config;

import java.util.Locale;
import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import lombok.Getter;
import lombok.Setter;
import nl.rijnijssel.scrumit.config.nested.AdminConfiguration;
import nl.rijnijssel.scrumit.config.nested.SheetsConfiguration;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.NestedConfigurationProperty;

@ConfigurationProperties("scrumit")
@Getter
@Setter
public class ScrumitConfiguration {

    /**
     * Path to Google Sheets API service account.
     */
    @Valid
    @NestedConfigurationProperty
    private SheetsConfiguration sheets;

    /**
     * Default locale for ScrumIT.
     */
    private Locale defaultLocale = Locale.forLanguageTag("nl");

    @Valid
    @NestedConfigurationProperty
    private AdminConfiguration admin;

    @Min(0)
    @Max(65535)
    private int httpPort;
}
