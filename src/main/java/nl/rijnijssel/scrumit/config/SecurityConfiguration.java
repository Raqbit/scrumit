/*
 * Scrumit
 * Copyright (C) 2019  Bram Ceulemans (bramceulemans@me.com)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

package nl.rijnijssel.scrumit.config;

import static nl.rijnijssel.scrumit.application.ScrumitConstants.LOGIN_FAILURE_URL;
import static nl.rijnijssel.scrumit.application.ScrumitConstants.LOGIN_URL;

import nl.rijnijssel.scrumit.application.ScrumitConstants;
import nl.rijnijssel.scrumit.domain.Role;
import nl.rijnijssel.scrumit.web.security.AuthSuccessHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    private final UserDetailsService userDetailsService;
    private final PasswordEncoder passwordEncoder;
    private final AuthSuccessHandler successHandler;
    private String[] publicUrls = new String[]{
        "/css/**",
        "/icons/**",
        "/js/**",
        "/VAADIN/**",
        "/vaadinServlet/**",
        "/manifest.json"
    };

    /**
     * Security config constructor.
     */
    @Autowired
    public SecurityConfiguration(AuthSuccessHandler successHandler,
        UserDetailsService userDetailsService,
        PasswordEncoder passwordEncoder) {
        this.successHandler = successHandler;
        this.userDetailsService = userDetailsService;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable();

        http.authorizeRequests()
            .antMatchers(publicUrls).permitAll()
            .anyRequest().hasAnyAuthority(Role.getAll());

        http.formLogin()
            .permitAll()
            .usernameParameter("email")
            .passwordParameter("password")
            .loginPage(LOGIN_URL)
            .loginProcessingUrl(LOGIN_URL)
            .successHandler(successHandler)
            .failureUrl(LOGIN_FAILURE_URL);

        http.logout()
            .logoutSuccessUrl(ScrumitConstants.LOGOUT_URL);
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        super.configure(auth);
        auth.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder);
    }


}
