/*
 * Scrumit
 * Copyright (C) 2018  Bram Ceulemans (bramceulemans@me.com)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

package nl.rijnijssel.scrumit.domain;

public final class Role {

    public static final String ADMIN = "ADMIN";
    public static final String COACH = "COACH";
    public static final String STUDENT = "STUDENT";

    private Role() {
        // Should not be constructed
    }

    /**
     * Get all roles.
     *
     * @return all user roles
     */
    public static String[] getAll() {
        return new String[]{ADMIN, COACH, STUDENT};
    }
}
