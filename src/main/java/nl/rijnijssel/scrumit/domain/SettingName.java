/*
 * Scrumit
 * Copyright (C) 2018  Bram Ceulemans (bramceulemans@me.com)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

package nl.rijnijssel.scrumit.domain;

public enum SettingName {

    SPREADSHEETS_ENABLED("Whether the Google Sheets integration should be enabled", false),
    SPREADSHEET_ID("Put the spreadsheet for this cycle here",
        "1PXXFWi6PRgu98ZpES2MRQICcAqMCmhIUkDaBl3eOaio"),
    STUDENT_DEFAULT_PASSWORD("Default password when a student gets imported", "Welkom01"),
    STUDENT_DEFAULT_EMAIL_DOMAIN("Default email domain when a student gets imported",
        "rijnijssel.nl"),
    COACH_DEFAULT_PASSWORD("Default password for a coach.", "rijnijssel4thewin01"),
    COACH_DEFAULT_EMAIL_DOMAIN("Default email domain when a coach email is generated",
        "rijnijssel.nl");

    private final String description;
    private final String defaultValue;

    SettingName(String description, Object defaultValue) {
        this.description = description;
        this.defaultValue = String.valueOf(defaultValue);
    }

    public String getDescription() {
        return description;
    }

    public String getDefaultValue() {
        return defaultValue;
    }
}
