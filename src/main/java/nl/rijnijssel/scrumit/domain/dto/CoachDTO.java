/*
 * Scrumit
 * Copyright (C) 2019  Bram Ceulemans (bramceulemans@me.com)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

package nl.rijnijssel.scrumit.domain.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@Data
public class CoachDTO extends BaseDTO {

    private UserDTO user;
    private String abbreviation;

    public CoachDTO(String abbreviation, UserDTO user) {
        this.abbreviation = abbreviation;
        this.user = user;
    }

    public static String getFormattedName(CoachDTO coach) {
        if (coach == null) {
            // TODO: Add translation for "None"
            return "";
        }
        UserDTO user = coach.getUser();
        if (user == null) {
            return coach.getAbbreviation();
        }
        return user.getName();
    }
}
