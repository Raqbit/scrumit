/*
 * Scrumit
 * Copyright (C) 2019  Bram Ceulemans (bramceulemans@me.com)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

package nl.rijnijssel.scrumit.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import nl.rijnijssel.scrumit.domain.Project;

/**
 * Student data transfer object.
 *
 * @see nl.rijnijssel.scrumit.domain.entity.StudentEntity
 */
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
@Data
public class StudentDTO extends BaseDTO {

    private String sid;
    private UserDTO user;
    private String status;
    private String clazz;
    private int year;
    private CoachDTO coach;
    private String table;
    private String tableThursday;
    private Project project;

    /**
     * Constructor for {@link StudentDTO}.
     */
    public StudentDTO(String sid, UserDTO user, String status, String clazz, int year,
        CoachDTO coach) {
        this.sid = sid;
        this.user = user;
        this.status = status;
        this.clazz = clazz;
        this.year = year;
        this.coach = coach;
    }

    public static String getFormattedEmail(StudentDTO studentDTO) {
        if (studentDTO == null) {
            return "";
        }
        UserDTO user = studentDTO.getUser();
        if (user == null) {
            return "";
        }
        String email = user.getEmail();
        return email != null ? email : "";
    }
}
