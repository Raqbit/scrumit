/*
 * Scrumit
 * Copyright (C) 2018  Bram Ceulemans (bramceulemans@me.com)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

package nl.rijnijssel.scrumit.domain.entity;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import nl.rijnijssel.scrumit.domain.ActionType;

@Entity
@Table(name = "actions")
@Getter
@Setter
@ToString
public class ActionEntity extends BaseEntity {

    @Enumerated(EnumType.STRING)
    private ActionType type;

    private String details;

    @ManyToOne
    private UserEntity user;

    public ActionEntity() {
    }

    /**
     * Constructor for action.
     *
     * @param type the action type
     * @param details the details for this action
     * @param user the user that triggered the action
     */
    public ActionEntity(ActionType type, String details, UserEntity user) {
        this.type = type;
        this.details = details;
        this.user = user;
    }
}
