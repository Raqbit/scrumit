/*
 * Scrumit
 * Copyright (C) 2018  Bram Ceulemans (bramceulemans@me.com)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

package nl.rijnijssel.scrumit.domain.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.validator.constraints.Length;

@Entity
@Table(name = "answers")
@Getter
@Setter
@ToString
@NoArgsConstructor
public class AnswerEntity extends BaseEntity {

    @NotEmpty(message = "Yesterday cannot be null.")
    @Length(min = 1, max = 1000, message = "Yesterday field cannot be empty.")
    @Column(name = "yesterday")
    private String yesterday;

    @NotEmpty(message = "Problem cannot be null.")
    @Length(min = 1, max = 1000, message = "Problem field cannot be empty.")
    @Column(name = "problem")
    private String problem;

    @NotEmpty(message = "Problem cannot be null.")
    @Length(min = 1, max = 1000, message = "Today field cannot be empty.")
    @Column(name = "today")
    private String today;

    @Length(max = 1000, message = "Support field cannot be over 1000 characters.")
    @Column(name = "support")
    private String support;

    @ManyToOne
    private CoachEntity supportCoach;

    @NotNull(message = "Owner field cannot be empty.")
    @ManyToOne(optional = false)
    private StudentEntity owner;

    public AnswerEntity(@NotNull StudentEntity owner) {
        this.owner = owner;
    }

    /**
     * Constructor for Answer, including setters for all parameters.
     *
     * @param yesterday the answer text for yesterday
     * @param problem the problem text for yesterday
     * @param today the answer text for today
     * @param support the support text question for today
     * @param owner the owner of this answer
     * @param supportCoach the optional coach to ask support from
     */
    public AnswerEntity(@NotNull String yesterday, @NotNull String problem, @NotNull String today,
        String support,
        CoachEntity supportCoach,
        @NotNull StudentEntity owner) {
        this.yesterday = yesterday;
        this.problem = problem;
        this.today = today;
        this.support = support;
        this.supportCoach = supportCoach;
        this.owner = owner;
    }
}
