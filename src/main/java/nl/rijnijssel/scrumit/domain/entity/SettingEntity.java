/*
 * Scrumit
 * Copyright (C) 2018  Bram Ceulemans (bramceulemans@me.com)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

package nl.rijnijssel.scrumit.domain.entity;

import javax.persistence.Entity;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "settings")
public class SettingEntity extends BaseEntity {

    @Getter
    @Setter
    private String settingName;

    private String value;

    @Getter
    @Setter
    private String description;

    public SettingEntity() {
        //
    }

    /**
     * Setting constructor, including setters for all fields.
     *
     * @param settingName the name of this setting
     * @param value the value of this setting
     * @param description the description of this setting
     */
    public SettingEntity(String settingName, String value, String description) {
        this.settingName = settingName;
        this.value = value;
        this.description = description;
    }

    public String getStringValue() {
        return value;
    }

    public void setStringValue(String value) {
        this.value = value;
    }

    public boolean getBooleanValue() {
        return Boolean.parseBoolean(value);
    }

    public void setBooleanValue(boolean value) {
        this.value = Boolean.toString(value);
    }
}
