/*
 * Scrumit
 * Copyright (C) 2018  Bram Ceulemans (bramceulemans@me.com)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

package nl.rijnijssel.scrumit.domain.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.validator.constraints.Length;

@Entity
@Table(name = "students")
@Getter
@Setter
@ToString
public class StudentEntity extends BaseEntity {

    @NotNull
    @Length(min = 1, max = 10)
    @Column(unique = true)
    private String sid;
    @OneToOne
    private UserEntity user;
    private String status;
    @Column(name = "class")
    private String clazz;
    private int year;
    @ManyToOne
    private CoachEntity coach;
    @Column(name = "table_name", length = 8)
    private String table;
    @Column(length = 8)
    private String tableThursday;
    @Column
    private String projectName;
    @Column
    private String projectLevel;
    @Column
    private String projectTech;
    @Column
    private String projectClient;
    @Column
    private String projectSupervisor;

    public StudentEntity() {
        //
    }

    /**
     * Constructor for Student, including setters for all the fields.
     *
     * @param sid student id
     * @param user user entity which is linked to this student
     * @param status the status of this student, e.g. orientation, development, export, exams
     * @param clazz the class of this student
     * @param year the year of the student
     * @param coach the coach this student is coached by
     */
    public StudentEntity(@NotNull String sid, UserEntity user, String status, String clazz,
        int year,
        CoachEntity coach) {
        this.sid = sid;
        this.user = user;
        this.status = status;
        this.clazz = clazz;
        this.year = year;
        this.coach = coach;
    }
}
