/*
 * Scrumit
 * Copyright (C) 2018  Bram Ceulemans (bramceulemans@me.com)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

package nl.rijnijssel.scrumit.domain.entity;

import java.util.Locale;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import nl.rijnijssel.scrumit.application.annotation.RolePattern;

@Entity
@Table(name = "users")
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@Data
public class UserEntity extends BaseEntity {

    @NotNull
    @Size(min = 1, max = 255)
    private String name;

    @NotNull
    @Email
    @Size(min = 1, max = 255)
    @Column(unique = true)
    private String email;

    @NotNull
    @Size(min = 4, max = 255)
    private String password;

    @NotNull
    @Size(min = 1, max = 10)
    @RolePattern
    private String role;

    private Locale language;

    @NotNull
    private Long loginCount = 0L;

    private boolean locked = false;

    private boolean disabled = false;

    /**
     * User constructor, including setters for all fields.
     *
     * @param email the email for this user, used for logging in
     * @param name the full name of this user
     * @param password the encrypted (bcrypt) password of this user
     * @param role the role of this user
     * @see nl.rijnijssel.scrumit.domain.Role
     */
    public UserEntity(@NotNull String email, @NotNull String name, @NotNull String password,
        @NotNull String role) {
        this.name = name;
        this.email = email;
        this.password = password;
        this.role = role;
    }
}
