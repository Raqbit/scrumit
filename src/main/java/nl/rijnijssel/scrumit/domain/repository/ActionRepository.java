/*
 * Scrumit
 * Copyright (C) 2018  Bram Ceulemans (bramceulemans@me.com)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

package nl.rijnijssel.scrumit.domain.repository;

import java.util.List;
import nl.rijnijssel.scrumit.domain.ActionType;
import nl.rijnijssel.scrumit.domain.entity.ActionEntity;
import nl.rijnijssel.scrumit.domain.entity.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface ActionRepository extends JpaRepository<ActionEntity, Long> {

    /**
     * Find all actions from a certain user.
     *
     * @param user the {@link UserEntity} to search for
     * @return all actions that matched the query
     */
    @Query("select a from ActionEntity a where a.user=:user order by a.createdAt desc")
    List<ActionEntity> findActionsByUser(
        @Param("user") UserEntity user);

    /**
     * Find all actions from a certain user and type.
     *
     * @param user the {@link UserEntity} to search for
     * @param type the {@link ActionType} to search for
     * @return all actions that matched the query
     */
    @Query("select a from ActionEntity a where "
        + "a.user=:user and "
        + "a.type=:type "
        + "order by a.createdAt desc")
    List<ActionEntity> findActionsByUserAndType(
        @Param("user") UserEntity user,
        @Param("type") ActionType type);
}
