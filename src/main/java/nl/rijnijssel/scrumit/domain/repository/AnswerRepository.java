/*
 * Scrumit
 * Copyright (C) 2018  Bram Ceulemans (bramceulemans@me.com)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

package nl.rijnijssel.scrumit.domain.repository;

import java.util.List;
import nl.rijnijssel.scrumit.domain.entity.AnswerEntity;
import nl.rijnijssel.scrumit.domain.entity.StudentEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface AnswerRepository extends JpaRepository<AnswerEntity, Long> {

    /**
     * Find all answers given by a student.
     *
     * @param owner owner to search for
     * @return all answers that a given student has made
     */
    @Query("select a from AnswerEntity a where a.owner=:student order by a.createdAt desc")
    List<AnswerEntity> findByOwner(
        @Param("student") StudentEntity owner);

    long countByYesterdayLikeOrProblemLikeOrTodayLikeOrSupportLikeAllIgnoreCase(
        String yesterday, String problem, String today, String support);

    /**
     * See how many records match a certain query.
     *
     * @param filter the text to search for
     * @return amount of answers that match the query
     */
    default long count(String filter) {
        return countByYesterdayLikeOrProblemLikeOrTodayLikeOrSupportLikeAllIgnoreCase(
            filter, filter, filter, filter);
    }

    List<AnswerEntity> findByYesterdayLikeOrProblemLikeOrTodayLikeOrSupportLikeOrOwnerSidLikeOrOwnerUserNameLikeOrOwnerClazzLikeAllIgnoreCaseOrderByCreatedAtDesc(
        String yesterday, String problem, String today, String support, String ownerSid,
        String ownerUserName, String ownerClazz);

    /**
     * Get all answers that match a filter.
     *
     * @param filter the text to search for
     * @return answers that match the filter
     */
    default List<AnswerEntity> search(String filter) {
        return findByYesterdayLikeOrProblemLikeOrTodayLikeOrSupportLikeOrOwnerSidLikeOrOwnerUserNameLikeOrOwnerClazzLikeAllIgnoreCaseOrderByCreatedAtDesc(
            filter, filter, filter, filter, filter, filter, filter);
    }
}
