/*
 * Scrumit
 * Copyright (C) 2018  Bram Ceulemans (bramceulemans@me.com)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

package nl.rijnijssel.scrumit.domain.repository;

import java.util.List;
import nl.rijnijssel.scrumit.domain.entity.CoachEntity;
import nl.rijnijssel.scrumit.domain.entity.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CoachRepository extends JpaRepository<CoachEntity, Long> {

    /**
     * Find a coach by it's user.
     *
     * @param coachUser user to search for
     * @return coach that matches the user
     */
    CoachEntity findByUser(UserEntity coachUser);

    /**
     * Find a coach based on it's email address.
     *
     * @param userEmail email address to search for
     * @return the coach with that email address
     */
    CoachEntity findByUserEmail(String userEmail);

    /**
     * Find a coach based on it's abbreviation.
     *
     * @param abbreviation abbreviation to search for
     * @return the coach with that abbreviation
     */
    CoachEntity findByAbbreviation(String abbreviation);

    /**
     * See how many coaches match a search query.
     *
     * @param abbreviation abbreviation to search for
     * @param userEmail user email to search for
     * @param userName user name to search for
     * @return how many coaches match the filter
     */
    long countByAbbreviationLikeOrUserEmailLikeOrUserNameLikeAllIgnoreCase(
        String abbreviation, String userEmail, String userName);

    default long count(String query) {
        return countByAbbreviationLikeOrUserEmailLikeOrUserNameLikeAllIgnoreCase(
            query, query, query);
    }

    /**
     * Get all coaches matching a certain search query.
     *
     * @param abbreviation abbreviation to search for
     * @param userEmail user email to search for
     * @param userName user name to search for
     * @return all coaches that match the search query
     */
    List<CoachEntity> findByAbbreviationLikeOrUserEmailLikeOrUserNameLikeAllIgnoreCase(
        String abbreviation, String userEmail, String userName);

    default List<CoachEntity> search(String query) {
        return findByAbbreviationLikeOrUserEmailLikeOrUserNameLikeAllIgnoreCase(query, query,
            query);
    }

}
