/*
 * Scrumit
 * Copyright (C) 2018  Bram Ceulemans (bramceulemans@me.com)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

package nl.rijnijssel.scrumit.domain.repository;

import java.util.List;
import nl.rijnijssel.scrumit.domain.entity.SettingEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SettingRepository extends JpaRepository<SettingEntity, Long> {

    /**
     * Get a single setting.
     *
     * @param settingName setting name in group to search for
     * @return setting that matches the given group- and setting name
     */
    SettingEntity findBySettingName(String settingName);

    /**
     * See how many settings match a search query.
     *
     * @param settingName setting name to search for
     * @return amount of settings that match the given query
     */
    long countBySettingNameLikeAllIgnoreCase(String settingName);

    default long count(String query) {
        return countBySettingNameLikeAllIgnoreCase(query);
    }

    /**
     * Find a setting by it's name, not case sensitive.
     *
     * @param settingName setting name to search for
     * @return all settings that match the given query
     */
    List<SettingEntity> findBySettingNameLikeAllIgnoreCase(String settingName);

    default List<SettingEntity> search(String search) {
        return findBySettingNameLikeAllIgnoreCase(search);
    }

}
