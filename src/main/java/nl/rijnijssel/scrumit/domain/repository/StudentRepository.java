/*
 * Scrumit
 * Copyright (C) 2018  Bram Ceulemans (bramceulemans@me.com)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

package nl.rijnijssel.scrumit.domain.repository;

import java.util.List;
import nl.rijnijssel.scrumit.domain.entity.StudentEntity;
import nl.rijnijssel.scrumit.domain.entity.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface StudentRepository extends JpaRepository<StudentEntity, Long> {

    /**
     * Find a student entity by it's user.
     *
     * @param user user to search for
     * @return the student matching the given user
     */
    StudentEntity findByUser(UserEntity user);

    /**
     * Find a student entity by it's student id.
     *
     * @param sid student id to search for
     * @return the student matching the given student id
     */
    StudentEntity findStudentBySid(String sid);

    @Query(value = "SELECT sid FROM students", nativeQuery = true)
    List<String> findAllStudentIds();

    /**
     * See how many students match a search query.
     *
     * @param sid student id to search for
     * @param clazz class to search for
     * @param status status to search for
     * @param userName user name to search for
     * @return amount of students that match the given filter
     */
    long countBySidLikeOrClazzLikeOrStatusLikeOrUserNameLikeAllIgnoreCase(
        String sid, String clazz, String status, String userName);

    default long count(String query) {
        return countBySidLikeOrClazzLikeOrStatusLikeOrUserNameLikeAllIgnoreCase(
            query, query, query, query);
    }

    /**
     * Get all students that match a search query.
     *
     * @param sid student id to search for
     * @param clazz class to search for
     * @param status status to search for
     * @return all students that match the given filter
     */
    List<StudentEntity> findBySidLikeOrClazzLikeOrStatusLikeOrUserNameLikeAllIgnoreCase(
        String sid, String clazz, String status, String userName);

    default List<StudentEntity> search(String query) {
        return findBySidLikeOrClazzLikeOrStatusLikeOrUserNameLikeAllIgnoreCase(
            query, query, query, query);
    }

}
