/*
 * Scrumit
 * Copyright (C) 2018  Bram Ceulemans (bramceulemans@me.com)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

package nl.rijnijssel.scrumit.domain.repository;

import java.util.List;
import nl.rijnijssel.scrumit.domain.entity.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<UserEntity, Long> {

    /**
     * Find a user by it's email.
     *
     * @param email to search for
     * @return user which matches the email
     */
    UserEntity findByEmail(String email);

    /**
     * Find all users with a certain role.
     *
     * @param role role to search for
     * @return all users with a certain role
     */
    List<UserEntity> findByRole(String role);

    /**
     * Check how many users exist containing the following data.
     *
     * @param email email to search for
     * @param name name to search for
     * @return amount of users who match the criteria
     */
    long countByEmailLikeOrNameLikeAllIgnoreCase(String email, String name);

    default long count(String query) {
        return countByEmailLikeOrNameLikeAllIgnoreCase(query, query);
    }


    /**
     * Find all users that match a certain search query.
     *
     * @param email email to search for
     * @param name name to search for
     * @param role role to search for
     * @return users who match one of the filters
     */
    List<UserEntity> findByEmailLikeOrNameLikeOrRoleLikeAllIgnoreCase(
        String email, String name, String role);

    default List<UserEntity> search(String query) {
        return findByEmailLikeOrNameLikeOrRoleLikeAllIgnoreCase(query, query, query);
    }

}
