/*
 * Scrumit
 * Copyright (C) 2018  Bram Ceulemans (bramceulemans@me.com)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

package nl.rijnijssel.scrumit.domain.service;

import java.util.List;
import nl.rijnijssel.scrumit.application.MapTypes;
import nl.rijnijssel.scrumit.domain.dto.ActionDTO;
import nl.rijnijssel.scrumit.domain.entity.ActionEntity;
import nl.rijnijssel.scrumit.domain.repository.ActionRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ActionService implements CrudService<ActionEntity, ActionDTO> {

    private final ActionRepository repository;
    private final ModelMapper mapper;

    @Autowired
    public ActionService(ActionRepository repository, ModelMapper mapper) {
        this.repository = repository;
        this.mapper = mapper;
    }

    @Override
    public ActionRepository getRepository() {
        return repository;
    }

    @Override
    public ActionDTO save(ActionDTO dto) {
        ActionEntity entity = mapper.map(dto, ActionEntity.class);
        entity = getRepository().save(entity);
        return mapper.map(entity, ActionDTO.class);
    }

    @Override
    public ActionDTO get(long id) {
        return mapper.map(getRepository().getOne(id), ActionDTO.class);
    }

    @Override
    public List<ActionDTO> getAll() {
        return mapper.map(getRepository().findAll(), MapTypes.ACTION_DTO_LIST);
    }

    @Override
    public long countAnyMatching(String filter) {
        return 0;
    }

    @Override
    public List<ActionDTO> findAnyMatching(String filter) {
        return null;
    }
}
