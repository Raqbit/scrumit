/*
 * Scrumit
 * Copyright (C) 2018  Bram Ceulemans (bramceulemans@me.com)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

package nl.rijnijssel.scrumit.domain.service;

import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;
import nl.rijnijssel.scrumit.application.MapTypes;
import nl.rijnijssel.scrumit.domain.dto.AnswerDTO;
import nl.rijnijssel.scrumit.domain.dto.StudentDTO;
import nl.rijnijssel.scrumit.domain.dto.UserDTO;
import nl.rijnijssel.scrumit.domain.entity.AnswerEntity;
import nl.rijnijssel.scrumit.domain.entity.StudentEntity;
import nl.rijnijssel.scrumit.domain.repository.AnswerRepository;
import nl.rijnijssel.scrumit.event.StudentAnsweredEvent;
import org.apache.commons.lang3.time.DateUtils;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

@Service
public class AnswerService implements CrudService<AnswerEntity, AnswerDTO> {

    private final AnswerRepository answerRepository;
    private final ApplicationEventPublisher publisher;
    private final ModelMapper mapper;

    /**
     * Constructor for {@link AnswerService}.
     *
     * @param answerRepository the answer repository
     * @param publisher the event publisher
     * @param mapper the model mapper
     */
    @Autowired
    public AnswerService(AnswerRepository answerRepository,
        ApplicationEventPublisher publisher, ModelMapper mapper) {
        this.answerRepository = answerRepository;
        this.publisher = publisher;
        this.mapper = mapper;
    }

    @Override
    public AnswerRepository getRepository() {
        return answerRepository;
    }

    /**
     * Get all answers from a user.
     *
     * @param owner owner to get answers for
     * @return all answers by a user
     */
    public List<AnswerDTO> findByOwner(StudentDTO owner) {
        StudentEntity student = mapper.map(owner, StudentEntity.class);
        return mapper.map(getRepository().findByOwner(student), MapTypes.ANSWER_DTO_LIST);
    }

    @Override
    @Transactional
    public AnswerDTO save(AnswerDTO dto) {
        publisher.publishEvent(new StudentAnsweredEvent(dto, dto.getOwner(), this));
        AnswerEntity entity = mapper.map(dto, AnswerEntity.class);
        entity = getRepository().save(entity);
        return mapper.map(entity, AnswerDTO.class);
    }

    @Override
    public AnswerDTO get(long id) {
        return mapper.map(getRepository().getOne(id), AnswerDTO.class);
    }

    @Override
    public List<AnswerDTO> getAll() {
        return mapper.map(getRepository().findAll(), MapTypes.ANSWER_DTO_LIST);
    }

    /**
     * Check if a certain user has already submitted a scrum.
     *
     * @param owner owner of the answers to check
     * @return true if there already is a scrum from the user
     */
    public boolean hasAnsweredToday(StudentDTO owner) {
        Date today = new Date();
        return findByOwner(owner)
            .stream()
            .anyMatch(answer -> DateUtils.isSameDay(today, answer.getCreatedAt()));
    }

    /**
     * Find answers by a search query and owner.
     *
     * @param filter filter string to search for
     * @param user user to search for
     * @return list of answers that match the current query
     */
    public List<AnswerDTO> findAnyMatchingForUser(String filter, UserDTO user) {
        return findAnyMatching(filter).stream()
            .filter(Objects::nonNull)
            .filter(a -> Objects.nonNull(a.getOwner().getUser()))
            .filter(a -> a.getOwner().getUser().equals(user))
            .collect(Collectors.toList());
    }

    /**
     * How many results match a certain search query. Search in the fields "yesterday", "problem",
     * "today", "support"
     *
     * @param filter filter string to search for
     * @return how many answers matched the search query
     */
    @Override
    public long countAnyMatching(String filter) {
        if (StringUtils.hasText(filter)) {
            String repositoryFilter = "%" + filter + "%";
            return getRepository().count(repositoryFilter);
        }
        return getRepository().count();
    }

    /**
     * Get all answers matching a certain search query.
     *
     * @param filter filter string to search for
     * @return all answers matching a certain search query
     */
    @Override
    public List<AnswerDTO> findAnyMatching(String filter) {
        if (StringUtils.hasText(filter)) {
            String repositoryFilter = "%" + filter + "%";
            List<AnswerEntity> result = getRepository().search(repositoryFilter);
            return mapper.map(result, MapTypes.ANSWER_DTO_LIST);
        }
        return getAll();
    }

    /**
     * Get a list of all tables which are in the answer list.
     *
     * @return the list of tables
     */

    public List<String> getTables() {
        return getTables(getAll());
    }

    /**
     * Get a list of all tables which are in the answer list.
     *
     * @param answers the answer list to get the tables from
     * @return the list of tables found in the answer list
     */
    private List<String> getTables(List<AnswerDTO> answers) {
        return answers.stream()
            .filter(Objects::nonNull)
            .filter(a -> a.getOwner() != null)
            .map(a -> a.getOwner().getTable())
            .filter(Objects::nonNull)
            .distinct()
            .collect(Collectors.toList());
    }

    /**
     * Filter a list of answers using a list of tables.
     *
     * @param answers the list of answers to filter
     * @param tables the tables to filter with
     * @return the filtered list of answers.
     */
    public List<AnswerDTO> filterAnswersByTables(List<AnswerDTO> answers, Set<String> tables) {
        if (tables == null || tables.isEmpty()) {
            return answers;
        }
        return answers.stream()
            .filter(Objects::nonNull)
            .filter(a -> Objects.nonNull(a.getOwner()))
            .filter(a -> {
                String table = a.getOwner().getTable();
                return tables.contains(table);
            })
            .collect(Collectors.toList());
    }


    /**
     * Filter a list of answers by the owner's projects name.
     *
     * @param answers the answers to filter
     * @param projects list of projects to filter with
     * @return the filtered answer list
     */
    public List<AnswerDTO> filterAnswersByProjects(List<AnswerDTO> answers, Set<String> projects) {
        if (projects == null || projects.isEmpty()) {
            return answers;
        }
        return answers.stream()
            .filter(Objects::nonNull)
            .filter(a -> Objects.nonNull(a.getOwner()))
            .filter(a -> Objects.nonNull(a.getOwner().getProject()))
            .filter(a -> projects.contains(a.getOwner().getProject().getName()))
            .collect(Collectors.toList());
    }

    /**
     * Get all distinct projects for the answer dto list.
     *
     * @return the list of distinct projects for all answers
     */
    public List<String> getProjects() {
        return getProjects(getAll());
    }

    /**
     * Get all distinct projects for the answer dto list.
     *
     * @param answers the answers to distinguish
     * @return the list of distinct projects for the given answer list
     */
    private List<String> getProjects(List<AnswerDTO> answers) {
        return answers.stream()
            .filter(Objects::nonNull)
            .filter(a -> a.getOwner() != null)
            .filter(a -> a.getOwner().getProject() != null)
            .map(a -> a.getOwner().getProject().getName())
            .filter(Objects::nonNull)
            .distinct()
            .collect(Collectors.toList());
    }
}
