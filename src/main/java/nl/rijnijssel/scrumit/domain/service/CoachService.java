/*
 * Scrumit
 * Copyright (C) 2018  Bram Ceulemans (bramceulemans@me.com)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

package nl.rijnijssel.scrumit.domain.service;

import java.util.List;
import nl.rijnijssel.scrumit.application.MapTypes;
import nl.rijnijssel.scrumit.domain.SettingName;
import nl.rijnijssel.scrumit.domain.dto.CoachDTO;
import nl.rijnijssel.scrumit.domain.dto.UserDTO;
import nl.rijnijssel.scrumit.domain.entity.CoachEntity;
import nl.rijnijssel.scrumit.domain.entity.UserEntity;
import nl.rijnijssel.scrumit.domain.repository.CoachRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

@Service
public class CoachService implements CrudService<CoachEntity, CoachDTO> {

    private final CoachRepository coachRepository;
    private final SettingService settingService;
    private final PasswordEncoder passwordEncoder;
    private final ModelMapper mapper;


    /**
     * CoachService constructor for dependency injection.
     *
     * @param coachRepository the coach repository
     */
    @Autowired
    public CoachService(CoachRepository coachRepository, SettingService settingService,
        PasswordEncoder passwordEncoder, ModelMapper mapper) {
        this.coachRepository = coachRepository;
        this.settingService = settingService;
        this.passwordEncoder = passwordEncoder;
        this.mapper = mapper;
    }

    /**
     * Get the repository for coaches.
     *
     * @return the coach repository
     */
    @Override
    public CoachRepository getRepository() {
        return coachRepository;
    }

    @Override
    public CoachDTO save(CoachDTO dto) {
        CoachEntity entity = mapper.map(dto, CoachEntity.class);
        entity = getRepository().save(entity);
        return mapper.map(entity, CoachDTO.class);
    }

    @Override
    public CoachDTO get(long id) {
        return mapper.map(getRepository().getOne(id), CoachDTO.class);
    }

    @Override
    public List<CoachDTO> getAll() {
        return mapper.map(getRepository().findAll(), MapTypes.COACH_DTO_LIST);
    }

    /**
     * Check if a Coach has a user.
     *
     * @param coach coach to check for
     * @return if the coach has a user
     */
    boolean hasUser(CoachDTO coach) {
        return coach.getUser() != null;
    }

    /**
     * See how many rows match the given search query.
     *
     * @param filter filter to search for
     * @return how many rows match the given filter
     */
    @Override
    public long countAnyMatching(String filter) {
        if (StringUtils.hasText(filter)) {
            String repositoryFilter = "%" + filter + "%";
            return getRepository().count(repositoryFilter);
        }
        return getRepository().count();
    }

    /**
     * Get all rows that match a certain search query.
     *
     * @param filter filter to search for
     * @return all rows that match the given filter
     */
    @Override
    public List<CoachDTO> findAnyMatching(String filter) {
        if (StringUtils.hasText(filter)) {
            String repositoryFilter = "%" + filter + "%";
            List<CoachEntity> result = getRepository().search(repositoryFilter);
            return mapper.map(result, MapTypes.COACH_DTO_LIST);
        }
        return getAll();
    }

    /**
     * Generate a default password for a coach.
     *
     * @return the encoded default password
     */
    public String generateDefaultPassword() {
        final String defaultPassword = settingService
            .getStringSetting(SettingName.COACH_DEFAULT_PASSWORD);
        return passwordEncoder.encode(defaultPassword);
    }

    /**
     * Find a coach based on it's abbreviation.
     *
     * @param abbreviation the abbreviation to search for
     * @return the found coach
     */
    public CoachDTO findByAbbreviation(String abbreviation) {
        return mapper.map(getRepository().findByAbbreviation(abbreviation), CoachDTO.class);
    }

    /**
     * Find a coach by it's user.
     *
     * @param user the user to search for
     * @return the found coach
     */
    public CoachDTO findByUser(UserDTO user) {
        UserEntity entity = mapper.map(user, UserEntity.class);
        return mapper.map(getRepository().findByUser(entity), CoachDTO.class);
    }
}
