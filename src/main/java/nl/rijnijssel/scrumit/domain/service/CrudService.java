/*
 * Scrumit
 * Copyright (C) 2018  Bram Ceulemans (bramceulemans@me.com)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

package nl.rijnijssel.scrumit.domain.service;

import java.util.List;
import nl.rijnijssel.scrumit.domain.dto.BaseDTO;
import nl.rijnijssel.scrumit.domain.entity.BaseEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

@Transactional
public interface CrudService<E extends BaseEntity, D extends BaseDTO> {

    /**
     * Get the repository for this entity.
     *
     * @return the repository for this entity
     */
    JpaRepository<E, Long> getRepository();

    /**
     * Save an entity.
     *
     * @param entity entity to be saved
     * @return the saved entity
     */
    D save(D entity);

    /**
     * Delete an entity by it's ID.
     *
     * @param id entity ID to delete
     */
    default void delete(long id) {
        getRepository().deleteById(id);
    }

    /**
     * Get a certain entity by it's ID.
     *
     * @param id get a single entity using it's ID
     * @return the entity
     */
    D get(long id);

    /**
     * Get all items from the repository.
     *
     * @return all the items.
     */
    List<D> getAll();

    /**
     * See how many rows match the given search query.
     *
     * @param filter filter to search for
     * @return how many rows match the given filter
     */
    long countAnyMatching(String filter);

    /**
     * Get all rows that match a certain search query.
     *
     * @param filter filter to search for
     * @return all rows that match the given filter
     */
    List<D> findAnyMatching(String filter);

}
