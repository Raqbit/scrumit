/*
 * Scrumit
 * Copyright (C) 2018  Bram Ceulemans (bramceulemans@me.com)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

package nl.rijnijssel.scrumit.domain.service;

import java.util.List;
import nl.rijnijssel.scrumit.application.MapTypes;
import nl.rijnijssel.scrumit.domain.SettingName;
import nl.rijnijssel.scrumit.domain.dto.SettingDTO;
import nl.rijnijssel.scrumit.domain.entity.SettingEntity;
import nl.rijnijssel.scrumit.domain.repository.SettingRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

@Service
public class SettingService implements CrudService<SettingEntity, SettingDTO> {

    private final SettingRepository repository;
    private final ModelMapper mapper;

    /**
     * SettingService constructor.
     *
     * @param repository the setting repository
     */
    @Autowired
    public SettingService(SettingRepository repository, ModelMapper mapper) {
        this.repository = repository;
        this.mapper = mapper;
    }

    @Override
    public SettingRepository getRepository() {
        return repository;
    }

    @Override
    public SettingDTO save(SettingDTO dto) {
        SettingEntity entity = mapper.map(dto, SettingEntity.class);
        entity = getRepository().save(entity);
        return mapper.map(entity, SettingDTO.class);
    }

    @Override
    public SettingDTO get(long id) {
        return mapper.map(getRepository().getOne(id), SettingDTO.class);
    }

    @Override
    public List<SettingDTO> getAll() {
        return mapper.map(getRepository().findAll(), MapTypes.SETTING_DTO_LIST);
    }

    /**
     * Get the string value for a system setting.
     *
     * @param name the setting name enum
     * @return the setting string value
     */
    public String getStringSetting(SettingName name) {
        SettingDTO setting = getSetting(name);
        return setting.getStringValue();
    }

    /**
     * Get the boolean value for a system setting.
     *
     * @param name the setting name enum
     * @return the setting boolean value
     */
    public boolean getBooleanSetting(SettingName name) {
        SettingDTO setting = getSetting(name);
        return setting.getBooleanValue();
    }

    /**
     * Get a setting by it's enum.
     *
     * @param settingEnum the setting enum
     * @return the setting entity
     */
    public SettingDTO getSetting(SettingName settingEnum) {
        SettingEntity setting = getRepository().findBySettingName(settingEnum.name());
        if (setting != null) {
            return mapper.map(setting, SettingDTO.class);
        }
        return new SettingDTO();
    }

    /**
     * See how many settings match the given search query.
     *
     * @param filter filter to search for
     * @return how many settings match the given filter
     */
    @Override
    public long countAnyMatching(String filter) {
        if (StringUtils.hasText(filter)) {
            String repositoryFilter = "%" + filter + "%";
            return getRepository().count(repositoryFilter);
        }
        return getRepository().count();
    }

    /**
     * Get all settings that match a certain search query.
     *
     * @param filter filter to search for
     * @return all settings that match the given filter
     */
    @Override
    public List<SettingDTO> findAnyMatching(String filter) {
        if (StringUtils.hasText(filter)) {
            String repositoryFilter = "%" + filter + "%";
            List<SettingEntity> result = getRepository().search(repositoryFilter);
            return mapper.map(result, MapTypes.SETTING_DTO_LIST);
        }
        return getAll();
    }


    /**
     * Create all missing settings.
     */
    public void createMissingSettings() {
        for (SettingName defaultSetting : SettingName.values()) {
            String name = defaultSetting.name();
            SettingEntity setting = repository.findBySettingName(name);
            if (setting == null) {
                setting = new SettingEntity(name, defaultSetting.getDefaultValue(),
                    defaultSetting.getDescription());
                repository.save(setting);
            }
        }
    }
}
