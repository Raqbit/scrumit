/*
 * Scrumit
 * Copyright (C) 2018  Bram Ceulemans (bramceulemans@me.com)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

package nl.rijnijssel.scrumit.domain.service;

import java.util.List;
import nl.rijnijssel.scrumit.application.MapTypes;
import nl.rijnijssel.scrumit.domain.dto.StudentDTO;
import nl.rijnijssel.scrumit.domain.dto.UserDTO;
import nl.rijnijssel.scrumit.domain.entity.StudentEntity;
import nl.rijnijssel.scrumit.domain.entity.UserEntity;
import nl.rijnijssel.scrumit.domain.repository.StudentRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

@Service
public class StudentService implements CrudService<StudentEntity, StudentDTO> {

    private final StudentRepository studentRepository;
    private final ModelMapper mapper;

    /**
     * StudentService constructor.
     *
     * @param studentRepository the student repository
     */
    @Autowired
    public StudentService(StudentRepository studentRepository, ModelMapper mapper) {
        this.studentRepository = studentRepository;
        this.mapper = mapper;
    }

    /**
     * Get the student repository.
     *
     * @return the student repository
     */
    @Override
    public StudentRepository getRepository() {
        return studentRepository;
    }

    @Override
    @Transactional
    public StudentDTO save(StudentDTO dto) {
        StudentEntity entity = mapper.map(dto, StudentEntity.class);
        entity = getRepository().save(entity);
        return mapper.map(entity, StudentDTO.class);
    }

    @Cacheable(value = "students", key = "{id: #id}")
    @Override
    public StudentDTO get(long id) {
        return mapper.map(getRepository().getOne(id), StudentDTO.class);
    }

    @Override
    public List<StudentDTO> getAll() {
        return mapper.map(getRepository().findAll(), MapTypes.STUDENT_DTO_LIST);
    }

    /**
     * See how many students match a search query.
     *
     * @param filter search query
     * @return amount of students that match the given filter
     */
    @Override
    public long countAnyMatching(String filter) {
        if (StringUtils.hasText(filter)) {
            String repositoryFilter = "%" + filter + "%";
            return getRepository().count(repositoryFilter);
        } else {
            return getRepository().count();
        }
    }

    /**
     * Get all students that match a search query.
     *
     * @param filter search query
     * @return all students that match the given filter
     */
    @Override
    public List<StudentDTO> findAnyMatching(String filter) {
        if (StringUtils.hasText(filter)) {
            String repositoryFilter = "%" + filter + "%";
            List<StudentEntity> result = getRepository().search(repositoryFilter);
            return mapper.map(result, MapTypes.STUDENT_DTO_LIST);
        }
        return getAll();
    }

    public StudentDTO findByUser(UserDTO user) {
        UserEntity entity = mapper.map(user, UserEntity.class);
        return mapper.map(getRepository().findByUser(entity), StudentDTO.class);
    }
}
