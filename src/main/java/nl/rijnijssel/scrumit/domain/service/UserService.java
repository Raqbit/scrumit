/*
 * Scrumit
 * Copyright (C) 2018  Bram Ceulemans (bramceulemans@me.com)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

package nl.rijnijssel.scrumit.domain.service;

import java.util.List;
import nl.rijnijssel.scrumit.application.MapTypes;
import nl.rijnijssel.scrumit.domain.dto.UserDTO;
import nl.rijnijssel.scrumit.domain.entity.UserEntity;
import nl.rijnijssel.scrumit.domain.repository.UserRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

@Service
public class UserService implements CrudService<UserEntity, UserDTO> {

    private static final String MODIFY_LOCKED_USER_NOT_PERMITTED =
        "User has been locked and cannot be modified or deleted";

    private final UserRepository userRepository;
    private final ModelMapper mapper;

    @Autowired
    public UserService(UserRepository userRepository, ModelMapper mapper) {
        this.userRepository = userRepository;
        this.mapper = mapper;
    }

    @Override
    public UserRepository getRepository() {
        return userRepository;
    }

    /**
     * Safe saves a user dto, throws exception if the user is locked.
     *
     * @param dto user entity to save
     * @return the user that has been saved
     * @throws UnsupportedOperationException when the user that's being saved is locked.
     */
    @Override
    @CachePut(value = "students", key = "{id: #result.id}")
    @Transactional
    public UserDTO save(UserDTO dto) {
        throwIfUserLocked(dto.getId());
        UserEntity entity = mapper.map(dto, UserEntity.class);
        entity = getRepository().save(entity);
        return mapper.map(entity, UserDTO.class);
    }

    /**
     * Safe delete a user, throws exception if the user is locked.
     *
     * @param userId user ID to delete
     * @throws UnsupportedOperationException when the user that's being deleted is locked
     */
    @Override
    @CacheEvict(value = "students", key = "{id: #userId}")
    public void delete(long userId) {
        throwIfUserLocked(userId);
        getRepository().deleteById(userId);
    }

    @Cacheable(value = "students", key = "{id: #id}")
    @Transactional(readOnly = true)
    @Override
    public UserDTO get(long id) {
        return mapper.map(getRepository().getOne(id), UserDTO.class);
    }

    @Cacheable(value = "students")
    @Transactional(readOnly = true)
    @Override
    public List<UserDTO> getAll() {
        return mapper.map(getRepository().findAll(), MapTypes.USER_DTO_LIST);
    }

    /**
     * Throw an exception when the user matching the userId is locked.
     *
     * @param userId user id to check
     * @throws UnsupportedOperationException when a user is locked
     */
    private void throwIfUserLocked(Long userId) {
        if (userId == null) {
            return;
        }
        UserEntity dbUser = getRepository().findById(userId).orElse(null);
        if (dbUser != null && dbUser.isLocked()) {
            throw new UnsupportedOperationException(MODIFY_LOCKED_USER_NOT_PERMITTED);
        }
    }

    /**
     * See how many users match a certain search query.
     *
     * @param filter search query
     * @return how many users match the search query
     */
    @Override
    public long countAnyMatching(String filter) {
        if (StringUtils.hasText(filter)) {
            String repositoryFilter = "%" + filter + "%";
            return getRepository().count(repositoryFilter);
        } else {
            return getRepository().count();
        }
    }

    /**
     * Get all users matching a certain search query.
     *
     * @param filter search query
     * @return all users that match the search query
     */
    @Cacheable(value = "students", key = "{filter: #filter}")
    @Override
    public List<UserDTO> findAnyMatching(String filter) {
        if (StringUtils.hasText(filter)) {
            String repositoryFilter = "%" + filter + "%";
            List<UserEntity> result = getRepository().search(repositoryFilter);
            return mapper.map(result, MapTypes.USER_DTO_LIST);
        } else {
            return getAll();
        }
    }

    /**
     * Check if a certain user can login.
     *
     * @param user user to check
     * @return whether the user can authenticate with ScrumIT
     */
    public boolean canAuthenticate(UserDTO user) {
        return user == null || StringUtils.isEmpty(user.getEmail()) || !user.isDisabled();
    }

    /**
     * Find a user by it's email address.
     *
     * @param email the email to search for
     * @return user dto found using the email address or null if it doesn't exist
     */
    @Cacheable(value = "students", key = "{email: #email}")
    @Transactional(readOnly = true)
    public UserDTO findByEmail(String email) {
        UserEntity entity = getRepository().findByEmail(email);
        if (entity == null) {
            return null;
        }
        return mapper.map(entity, UserDTO.class);
    }
}
