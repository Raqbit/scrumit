/*
 * Scrumit
 * Copyright (C) 2018  Bram Ceulemans (bramceulemans@me.com)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

package nl.rijnijssel.scrumit.event;

import nl.rijnijssel.scrumit.domain.dto.AnswerDTO;
import nl.rijnijssel.scrumit.domain.dto.StudentDTO;
import org.springframework.context.ApplicationEvent;

/**
 * Triggered whenever a scrum is answered.
 *
 * @author Bram Ceulemans
 */
public class StudentAnsweredEvent extends ApplicationEvent {

    private final AnswerDTO answer;
    private final StudentDTO student;

    /**
     * Create a new ApplicationEvent.
     *
     * @param answer the answer dto
     * @param student the student dto
     * @param source the object on which the event initially occurred (never {@code null})
     */
    public StudentAnsweredEvent(AnswerDTO answer, StudentDTO student, Object source) {
        super(source);
        this.answer = answer;
        this.student = student;
    }

    public AnswerDTO getAnswer() {
        return answer;
    }

    public StudentDTO getStudent() {
        return student;
    }
}
