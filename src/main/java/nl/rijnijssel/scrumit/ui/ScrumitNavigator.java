/*
 * Scrumit
 * Copyright (C) 2019  Bram Ceulemans (bramceulemans@me.com)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

package nl.rijnijssel.scrumit.ui;

import com.vaadin.navigator.View;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.spring.annotation.UIScope;
import com.vaadin.spring.internal.Conventions;
import com.vaadin.spring.navigator.SpringNavigator;
import nl.rijnijssel.scrumit.domain.Role;
import nl.rijnijssel.scrumit.ui.view.AnswerView;
import nl.rijnijssel.scrumit.ui.view.DashboardView;
import nl.rijnijssel.scrumit.web.security.SecurityUtils;
import org.springframework.stereotype.Component;

@UIScope
@Component
public class ScrumitNavigator extends SpringNavigator {

    /**
     * Get the view name of a certain class.
     *
     * @param view view class to get name from
     * @return name of the view
     */
    private String getViewName(Class<? extends View> view) {
        SpringView springView = view.getAnnotation(SpringView.class);
        if (springView == null) {
            throw new IllegalArgumentException("The target class must be a @SpringView");
        }

        return Conventions.deriveMappingForView(view, springView);
    }

    /**
     * Navigate to a certain view.
     *
     * @param targetView view to navigate to
     */
    public void navigateTo(Class<? extends View> targetView) {
        String viewName = getViewName(targetView);
        navigateTo(viewName);
    }

    /**
     * Navigate to a certain view, with parameters.
     *
     * @param targetView view to navigate to
     */
    public void navigateTo(Class<? extends View> targetView, Object parameter) {
        String viewId = getViewName(targetView);
        navigateTo(viewId + "/" + parameter.toString());
    }

    /**
     * Navigate to the default view for your role.
     */
    void navigateToDefaultView() {
        if (getState() != null && !getState().isEmpty()) {
            return;
        }

        if (SecurityUtils.isCurrentUserInRole(Role.ADMIN)) {
            navigateTo(DashboardView.class);
        } else if (SecurityUtils.isCurrentUserInRole(Role.COACH)) {
            navigateTo(DashboardView.class);
        } else if (SecurityUtils.isCurrentUserInRole(Role.STUDENT)) {
            navigateTo(AnswerView.class);
        }
    }
}
