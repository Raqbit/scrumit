/*
 * Scrumit
 * Copyright (C) 2019  Bram Ceulemans (bramceulemans@me.com)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

package nl.rijnijssel.scrumit.ui;

import com.github.appreciated.material.MaterialTheme;
import com.vaadin.annotations.Theme;
import com.vaadin.annotations.Title;
import com.vaadin.annotations.Viewport;
import com.vaadin.navigator.PushStateNavigation;
import com.vaadin.navigator.ViewLeaveAction;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.WebBrowser;
import com.vaadin.shared.Position;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.spring.navigator.SpringViewProvider;
import com.vaadin.ui.Notification;
import com.vaadin.ui.UI;
import java.net.URI;
import java.util.Locale;
import lombok.extern.slf4j.Slf4j;
import nl.rijnijssel.scrumit.application.I18n;
import nl.rijnijssel.scrumit.application.ScrumitConstants;
import nl.rijnijssel.scrumit.domain.Role;
import nl.rijnijssel.scrumit.domain.dto.UserDTO;
import nl.rijnijssel.scrumit.ui.container.MainView;
import nl.rijnijssel.scrumit.ui.view.AccessDeniedView;
import nl.rijnijssel.scrumit.web.security.SecurityUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.vaadin.leif.headertags.Link;
import org.vaadin.leif.headertags.LinkTags;
import org.vaadin.leif.headertags.Meta;
import org.vaadin.leif.headertags.MetaTags;

@SpringUI(path = "/app")
@Title("ScrumIT")
@Theme("scrumit")
@Secured({Role.STUDENT, Role.COACH, Role.ADMIN})
@Viewport("width=device-width,initial-scale=1.0,user-scalable=no")
@PushStateNavigation
@LinkTags({
    @Link(rel = "manifest", href = "/manifest.json"),
    @Link(rel = "shortcut icon", href = "/icons/favicon.ico"),
    @Link(rel = "icon", href = "/icons/favicon-32x32.png", type = "image/png", sizes = "32x32"),
    @Link(rel = "icon", href = "/icons/favicon-16x16.png", type = "image/png", sizes = "16x16"),
    @Link(rel = "apple-touch-icon", href = "/icons/apple-touch-icon.png"),
})
@MetaTags({
    @Meta(name = "msapplication-TileColor", content = "#DA532C"),
    @Meta(name = "msapplication-config", content = "/icons/browserconfig.xml"),
    @Meta(name = "theme-color", content = "#FFFFFF"),
})
@Slf4j
public class ScrumitUI extends UI {

    private final SpringViewProvider viewProvider;
    private final ScrumitNavigator scrumitNavigator;
    private final MainView mainView;
    private Locale locale = I18n.getCurrentLocale();
    private UserDTO currentUser;

    /**
     * Scrumit UI constructor.
     *
     * @param viewProvider the view provider
     * @param scrumitNavigator the navigator
     * @param mainView the main view
     */
    @Autowired
    public ScrumitUI(SpringViewProvider viewProvider, ScrumitNavigator scrumitNavigator,
        MainView mainView) {
        this.scrumitNavigator = scrumitNavigator;
        this.viewProvider = viewProvider;
        this.mainView = mainView;
    }

    public static ScrumitUI getCurrent() {
        return (ScrumitUI) UI.getCurrent();
    }

    /**
     * Initialize the Vaadin UI.
     *
     * @param vaadinRequest the vaadin request
     */
    @Override
    protected void init(VaadinRequest vaadinRequest) {
        currentUser = SecurityUtils.getCurrentUser();
        logRequestInformation();
        getSession().setLocale(locale);
        setNavigator(scrumitNavigator);
        setErrorHandler(this::onError);
        setResponsive(true);
        setContent(mainView);
        viewProvider.setAccessDeniedViewClass(AccessDeniedView.class);
        scrumitNavigator.navigateToDefaultView();
    }

    /**
     * Print the request information associated with this UI.
     */
    private void logRequestInformation() {
        String email = currentUser != null ? currentUser.getEmail() : "";
        String scheme = getPage().getLocation().getScheme();
        String ssl = "https".equals(scheme) ? "Yes" : "No";
        URI location = getPage().getLocation();
        WebBrowser webBrowser = getPage().getWebBrowser();
        String browserName = webBrowser.getBrowserApplication();
        log.info("UI Initialization: ");
        log.info("| User: {}", email);
        log.info("| SSL: {}", ssl);
        log.info("| Locale: {}", locale);
        log.info("| Location: {}", location);
        log.info("| Browser: {}", browserName);
    }

    /**
     * Executed when an error occurs.
     *
     * @param event error event
     */
    private void onError(com.vaadin.server.ErrorEvent event) {
        Notification notification = new Notification(I18n.get("notification.error_message"));
        notification.setPosition(Position.TOP_RIGHT);
        notification.setStyleName(MaterialTheme.NOTIFICATION_ERROR);
        notification.show(getPage());
        log.error("General exception", event.getThrowable());
    }

    /**
     * Log the current user out.
     *
     * @see ScrumitConstants#LOGIN_URL
     * @see SecurityUtils#getCurrentUser()
     * @see SecurityUtils#getPrincipal()
     * @see #getSession()
     */
    public void logout() {
        logout(null);
    }

    /**
     * Log the current user out.
     *
     * @param appendString the string that should be appended to the LOGIN_URL
     * @see ScrumitConstants#LOGIN_URL
     * @see SecurityUtils#getCurrentUser()
     * @see SecurityUtils#getPrincipal()
     * @see #getSession()
     */
    public void logout(String appendString) {
        ViewLeaveAction logout = () -> {
            getSession().getSession().invalidate();
            String url = StringUtils.isBlank(appendString) ? "" : "?" + appendString;
            getPage().setLocation(ScrumitConstants.LOGIN_URL + url);
        };
        access(() -> getNavigator().runAfterLeaveConfirmation(logout));
    }
}

