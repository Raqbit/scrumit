/*
 * Scrumit
 * Copyright (C) 2019  Bram Ceulemans (bramceulemans@me.com)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

package nl.rijnijssel.scrumit.ui.component;

import com.github.appreciated.material.MaterialTheme;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import nl.rijnijssel.scrumit.application.I18n;
import nl.rijnijssel.scrumit.ui.model.AppInfo;

public class AboutCard extends VerticalLayout {

    /**
     * Construct the {@link AboutCard}
     *
     * @param info the {@link AppInfo}
     */
    public AboutCard(AppInfo info) {
        addStyleNames(MaterialTheme.CARD_1);
        TextField version = new TextField();
        version.setCaption(I18n.get("about.version"));
        version.setReadOnly(true);
        addComponent(version);
        if (info != null) {
            version.setValue(info.getVersion());
        }
    }
}
