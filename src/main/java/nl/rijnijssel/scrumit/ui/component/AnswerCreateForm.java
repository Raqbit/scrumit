/*
 * Scrumit
 * Copyright (C) 2019  Bram Ceulemans (bramceulemans@me.com)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

package nl.rijnijssel.scrumit.ui.component;

import static org.springframework.util.StringUtils.hasText;

import com.vaadin.data.Binder;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.TextArea;
import nl.rijnijssel.scrumit.application.I18n;
import nl.rijnijssel.scrumit.domain.dto.AnswerDTO;
import nl.rijnijssel.scrumit.domain.dto.CoachDTO;
import nl.rijnijssel.scrumit.domain.dto.UserDTO;
import org.vaadin.viritin.form.AbstractForm;
import org.vaadin.viritin.layouts.MFormLayout;

public class AnswerCreateForm extends AbstractForm<AnswerDTO> {

    private final TextArea problem = new TextArea(I18n.get("answers.grid.problem"));
    private final TextArea today = new TextArea(I18n.get("answers.grid.today"));
    private final TextArea support = new TextArea(I18n.get("answers.grid.support"));
    public final ComboBox<CoachDTO> coach = new ComboBox<>(
        I18n.get("answers.grid.support_coach"));
    private final TextArea yesterday = new TextArea(I18n.get("answers.grid.yesterday"));

    /**
     * Constructor for AnswerForm.
     */
    public AnswerCreateForm() {
        super(AnswerDTO.class);
        setModalWindowTitle(I18n.get("answers.form.title"));
        focusFirst();
    }

    /**
     * Format the coach abbreviation and user name.
     *
     * @param coach the coach to format
     * @return formatted coach name using the abbreviation and user name
     */
    private static String getCoachCaption(CoachDTO coach) {
        if (coach == null) {
            return null;
        }
        UserDTO user = coach.getUser();
        String abbreviation = coach.getAbbreviation();
        if (user != null && hasText(user.getName()) && hasText(abbreviation)) {
            return String.format("%s (%s)", user.getName(), abbreviation);
        }
        return coach.getAbbreviation();
    }

    @Override
    protected Component createContent() {
        yesterday.setSizeFull();
        yesterday.setRows(3);
        yesterday.setDescription(I18n.get("answers.form.yesterday_description"));
        problem.setSizeFull();
        problem.setRows(3);
        problem.setDescription(I18n.get("answers.form.problem_description"));
        today.setSizeFull();
        today.setRows(3);
        today.setDescription(I18n.get("answers.form.today_description"));
        support.setSizeFull();
        support.setRows(3);
        support.setDescription(I18n.get("answers.form.support_description"));
        coach.setSizeFull();
        coach.setItemCaptionGenerator(AnswerCreateForm::getCoachCaption);
        coach.setDescription(I18n.get("answers.form.support_coach_description"));

        return new MFormLayout(yesterday, problem, today, support, coach, getToolbar())
            .withMargin(true).withUndefinedSize();
    }

    @Override
    protected void bind() {
        Binder<AnswerDTO> binder = getBinder();
        binder
            .forField(yesterday)
            .asRequired(I18n.get("answers.form.yesterday_required"))
            .bind(AnswerDTO::getYesterday, AnswerDTO::setYesterday);
        binder
            .forField(problem)
            .asRequired(I18n.get("answers.form.problem_required"))
            .bind(AnswerDTO::getProblem, AnswerDTO::setProblem);
        binder
            .forField(today)
            .asRequired(I18n.get("answers.form.today_required"))
            .bind(AnswerDTO::getToday, AnswerDTO::setToday);
        binder.forField(support).bind(AnswerDTO::getSupport, AnswerDTO::setSupport);
        binder.forField(coach).bind(AnswerDTO::getSupportCoach, AnswerDTO::setSupportCoach);
        super.bind();
    }

}
