/*
 * Scrumit
 * Copyright (C) 2018  Bram Ceulemans (bramceulemans@me.com)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

package nl.rijnijssel.scrumit.ui.component;

import com.github.appreciated.material.MaterialTheme;
import com.vaadin.data.Binder;
import com.vaadin.ui.Label;
import com.vaadin.ui.TextField;
import javax.annotation.PostConstruct;
import nl.rijnijssel.scrumit.application.I18n;
import nl.rijnijssel.scrumit.application.annotation.PrototypeScope;
import nl.rijnijssel.scrumit.domain.dto.CoachDTO;
import nl.rijnijssel.scrumit.ui.container.component.ColoredLabel;
import org.springframework.stereotype.Component;
import org.vaadin.viritin.layouts.MFormLayout;

@Component
@PrototypeScope
public class CoachAccountForm extends MFormLayout {

    private Binder<CoachDTO> binder;

    /**
     * Set up all the fields, bind the fields and add them to the layout.
     */
    @PostConstruct
    private void postConstruct() {
        /* Create header */
        TextField abbreviation = new TextField(I18n.get("account.coach.abbreviation"));
        /* Bind all the fields */
        setupBinder(abbreviation);
        Label header = new ColoredLabel(I18n.get("account.coach.header"));
        /* Add the header and fields to the layout */
        with(header, abbreviation);
        withStyleName(MaterialTheme.CARD_1);
        withUndefinedSize();
    }

    /**
     * Set up the binder.
     *
     * @param abbreviation the abbreviation field
     */
    private void setupBinder(TextField abbreviation) {
        binder = new Binder<>();
        binder.forField(abbreviation).bind(CoachDTO::getAbbreviation, null);
    }

    /**
     * Set the coach for this form.
     *
     * @param coach the coach to set
     */
    public void setCoach(CoachDTO coach) {
        binder.readBean(coach);
    }
}
