/*
 * Scrumit
 * Copyright (C) 2018  Bram Ceulemans (bramceulemans@me.com)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

package nl.rijnijssel.scrumit.ui.component;

import com.vaadin.annotations.PropertyId;
import com.vaadin.data.validator.EmailValidator;
import com.vaadin.ui.Component;
import com.vaadin.ui.Window;
import nl.rijnijssel.scrumit.application.I18n;
import nl.rijnijssel.scrumit.domain.SettingName;
import nl.rijnijssel.scrumit.domain.dto.CoachDTO;
import nl.rijnijssel.scrumit.domain.service.SettingService;
import org.springframework.util.StringUtils;
import org.vaadin.viritin.fields.MTextField;
import org.vaadin.viritin.form.AbstractForm;
import org.vaadin.viritin.layouts.MFormLayout;

public class CoachEditForm extends AbstractForm<CoachDTO> {

    private final SettingService settingService;

    @PropertyId("user.name")
    private MTextField name;
    @PropertyId("user.email")
    private MTextField email;
    @PropertyId("abbreviation")
    private MTextField abbreviation;

    /**
     * Constructor for the coach editor.
     */
    public CoachEditForm(SettingService settingService) {
        super(CoachDTO.class);
        this.settingService = settingService;
        setModalWindowTitle(I18n.get("coaches.editor.title"));
    }

    /**
     * Gets called after construct, initializes all content.
     *
     * @return the formlayout with all components
     */
    @Override
    protected Component createContent() {
        name = new MTextField("Name");
        email = new MTextField("Email");
        abbreviation = new MTextField("Abbreviation");
        abbreviation.setReadOnly(true);
        name.addBlurListener(e -> {
            if (StringUtils.hasText(email.getValue())) {
                return;
            }
            String[] names = name.getValue().split(" ");
            if (names.length < 2) {
                return;
            }
            email.setValue(generateEmailFromName(name.getValue(), settingService.getStringSetting(
                SettingName.COACH_DEFAULT_EMAIL_DOMAIN)));
        });
        return new MFormLayout(name, email, abbreviation, getToolbar()).withUndefinedSize()
            .withMargin(true);
    }

    /**
     * Generate the email for a name. Using first letters of every word, and full last name.
     *
     * @param name the full name for the coach
     * @param domain domain name without the <code>@</code> symbol
     * @return the formatted email
     */
    private String generateEmailFromName(String name, String domain) {
        StringBuilder builder = new StringBuilder();
        String[] names = name.split(" ");
        for (int i = 0; i < names.length; i++) {
            if (i < (names.length - 1)) {
                builder.append(names[i], 0, 1).append('.');
            } else {
                builder.append(names[i]);
            }
        }
        builder.append("@");
        builder.append(domain);
        return builder.toString().toLowerCase();
    }

    /**
     * Bind all fields to their bean propertyies.
     */
    @Override
    protected void bind() {
        getBinder().forField(name)
            .asRequired(I18n.get("coaches.editor.name_required"))
            .bind("user.name");
        getBinder().forField(email)
            .asRequired(I18n.get("coaches.editor.email_required"))
            .withValidator(new EmailValidator(I18n.get("coaches.editor.email_invalid")))
            .bind("user.email");
        getBinder().forField(abbreviation)
            .bind("abbreviation");
    }

    /**
     * Opens the coach editor popup.
     *
     * @return the built window
     */
    @Override
    public Window openInModalPopup() {
        Window window = super.openInModalPopup();
        window.setResizable(false);
        window.setModal(false);
        return window;
    }
}
