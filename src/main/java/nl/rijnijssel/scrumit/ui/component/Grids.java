/*
 * Scrumit
 * Copyright (C) 2019  Bram Ceulemans (bramceulemans@me.com)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

package nl.rijnijssel.scrumit.ui.component;

import com.vaadin.shared.ui.grid.HeightMode;
import com.vaadin.spring.annotation.UIScope;
import com.vaadin.ui.Grid;
import com.vaadin.ui.Grid.SelectionMode;
import com.vaadin.ui.renderers.DateRenderer;
import nl.rijnijssel.scrumit.application.I18n;
import nl.rijnijssel.scrumit.application.ScrumitConstants;
import nl.rijnijssel.scrumit.domain.dto.AnswerDTO;
import nl.rijnijssel.scrumit.domain.dto.CoachDTO;
import nl.rijnijssel.scrumit.domain.dto.StudentDTO;
import nl.rijnijssel.scrumit.domain.dto.UserDTO;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class Grids {

    public static final String DASHBOARD = "dashboard";
    private static final String STUDENT = "student";
    public static final String ANSWER = "answer";
    private static final String COACH = "coach";

    private static String generateStudentName(StudentDTO student) {
        return student.getUser().getEmail();
    }

    /**
     * Coach DTO grid.
     *
     * @return grid with all columns.
     */
    @Bean
    @Qualifier(Grids.COACH)
    @UIScope
    public Grid<CoachDTO> coachGrid() {
        Grid<CoachDTO> grid = new Grid<>();
        grid.setSizeFull();
        grid.setSelectionMode(SelectionMode.SINGLE);
        grid.setHeightMode(HeightMode.UNDEFINED);
        grid.addColumn(CoachDTO::getUser, u -> {
            if (u == null) {
                return "";
            }
            return u.getName();
        })
            .setCaption(I18n.get("coaches.grid.name"));
        grid.addColumn(CoachDTO::getUser, u -> {
            if (u == null) {
                return "";
            }
            return u.getEmail();
        })
            .setCaption(I18n.get("coaches.grid.email"));
        grid.addColumn(CoachDTO::getAbbreviation)
            .setCaption(I18n.get("coaches.grid.abbreviation"));
        grid.addColumn(CoachDTO::getCreatedAt)
            .setRenderer(new DateRenderer(ScrumitConstants.DATE_FORMAT_LONG))
            .setCaption(I18n.get("coaches.grid.created_at"));
        grid.addColumn(CoachDTO::getUpdatedAt)
            .setRenderer(new DateRenderer(ScrumitConstants.DATE_FORMAT_LONG))
            .setCaption(I18n.get("coaches.grid.updated_at"));
        grid.getColumns().forEach(c -> c.setHidable(true));
        return grid;
    }

    /**
     * Answer DTO grid.
     *
     * @return grid with all columns.
     */
    @Bean
    @Qualifier(Grids.ANSWER)
    @UIScope
    public Grid<AnswerDTO> answerGrid() {
        Grid<AnswerDTO> grid = new Grid<>();
        grid.setSizeFull();
        grid.setHeightByRows(12);
        /* Add columns to the answer grid */
        grid.addColumn(AnswerDTO::getYesterday)
            .setId("yesterday")
            .setCaption(I18n.get("answers.grid.yesterday"));
        grid.addColumn(AnswerDTO::getProblem)
            .setId("problem")
            .setCaption(I18n.get("answers.grid.problem"));
        grid.addColumn(AnswerDTO::getToday)
            .setId("today")
            .setCaption(I18n.get("answers.grid.today"));
        grid.addColumn(AnswerDTO::getSupport)
            .setId("support")
            .setCaption(I18n.get("answers.grid.support"));
        grid.addColumn(AnswerDTO::getSupportCoach)
            .setId("coach")
            .setCaption(I18n.get("answers.grid.support_coach"));
        grid.addColumn(AnswerDTO::getCreatedAt)
            .setId("answered_at")
            .setCaption(I18n.get("answers.grid.answered_at"))
            .setRenderer(new DateRenderer(ScrumitConstants.DATE_FORMAT_LONG));
        grid.getColumns().forEach(c -> c.setHidable(true));
        return grid;
    }

    /**
     * Student DTO grid.
     *
     * @return grid with all columns.
     */
    @Bean
    @Qualifier(Grids.STUDENT)
    @UIScope
    public Grid<StudentDTO> studentGrid() {
        Grid<StudentDTO> grid = new Grid<>();
        grid.setSizeFull();
        grid.setHeightByRows(12);

        grid.addColumn(StudentDTO::getSid)
            .setCaption(I18n.get("students.grid.student_id"));
        grid.addColumn(StudentDTO::getUser, UserDTO::getName)
            .setCaption(I18n.get("students.grid.name"));
        grid.addColumn(StudentDTO::getFormattedEmail)
            .setCaption(I18n.get("students.grid.email"));
        grid.addColumn(StudentDTO::getClazz)
            .setCaption(I18n.get("students.grid.class"));
        grid.addColumn(StudentDTO::getStatus)
            .setCaption(I18n.get("students.grid.status"));
        grid.addColumn(StudentDTO::getCoach, CoachDTO::getFormattedName)
            .setCaption(I18n.get("students.grid.coach"));
        grid.addColumn(StudentDTO::getUser, u -> String.valueOf(u.getLoginCount()))
            .setCaption(I18n.get("students.grid.login_count"));
        grid.addColumn(StudentDTO::getUpdatedAt)
            .setRenderer(new DateRenderer(ScrumitConstants.DATE_FORMAT_LONG))
            .setCaption(I18n.get("students.grid.updated_at"));
        grid.addColumn(StudentDTO::getTable)
            .setCaption(I18n.get("students.grid.table"));
        grid.setFrozenColumnCount(1);
        grid.getColumns().forEach(column -> column.setHidable(true));
        return grid;
    }

    /**
     * Answer DTO (Dashboard) grid.
     *
     * @return grid with all columns.
     */
    @Bean
    @Qualifier(Grids.DASHBOARD)
    @UIScope
    public Grid<AnswerDTO> dashboardGrid() {
        Grid<AnswerDTO> grid = new Grid<>();
        grid.setSizeFull();
        grid.setHeightByRows(12);

        grid.addColumn(AnswerDTO::getOwner, studentDTO -> studentDTO.getUser().getName())
            .setCaption(I18n.get("dashboard.answer_grid.student"));
        grid.addColumn(AnswerDTO::getOwner, StudentDTO::getClazz)
            .setCaption(I18n.get("dashboard.answer_grid.class"));
        grid.addColumn(AnswerDTO::getYesterday)
            .setCaption(I18n.get("dashboard.answer_grid.yesterday"));
        grid.addColumn(AnswerDTO::getProblem)
            .setCaption(I18n.get("dashboard.answer_grid.problem"));
        grid.addColumn(AnswerDTO::getToday)
            .setCaption(I18n.get("dashboard.answer_grid.today"));
        grid.addColumn(AnswerDTO::getSupport)
            .setCaption(I18n.get("dashboard.answer_grid.support"));
        grid.addColumn(AnswerDTO::getSupportCoach, CoachDTO::getFormattedName)
            .setCaption(I18n.get("dashboard.answer_grid.support_coach"));
        grid.addColumn(AnswerDTO::getCreatedAt)
            .setCaption(I18n.get("dashboard.answer_grid.answered_at"))
            .setRenderer(new DateRenderer(ScrumitConstants.DATE_FORMAT_LONG))
            .setId("created");
        grid.getColumns().forEach(c -> c.setHidable(true));
        return grid;
    }
}
