/*
 * Scrumit
 * Copyright (C) 2019  Bram Ceulemans (bramceulemans@me.com)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

package nl.rijnijssel.scrumit.ui.component;

import com.github.appreciated.material.MaterialTheme;
import com.vaadin.data.Binder;
import com.vaadin.ui.Label;
import javax.annotation.PostConstruct;
import nl.rijnijssel.scrumit.application.I18n;
import nl.rijnijssel.scrumit.application.annotation.PrototypeScope;
import nl.rijnijssel.scrumit.domain.dto.CoachDTO;
import nl.rijnijssel.scrumit.domain.dto.StudentDTO;
import nl.rijnijssel.scrumit.ui.container.component.ColoredLabel;
import org.springframework.stereotype.Component;
import org.vaadin.viritin.fields.MTextField;
import org.vaadin.viritin.layouts.MFormLayout;

@Component
@PrototypeScope
public class StudentAccountForm extends MFormLayout {

    private Binder<StudentDTO> binder;

    /**
     * Create all the fields and setup of the binder.
     */
    @PostConstruct
    private void postConstruct() {
        /* Create all the fields needed for displaying student data */
        MTextField sid = new MTextField(I18n.get("account.student.student_id"));
        MTextField clazz = new MTextField(I18n.get("account.student.class"));
        MTextField coach = new MTextField(I18n.get("account.student.coach"));
        MTextField status = new MTextField(I18n.get("account.student.status"));
        setupBinder(sid, clazz, coach, status);
        Label header = new ColoredLabel(I18n.get("account.student.header"));
        /* Add all fields to the layout */
        with(header, sid, clazz, coach, status);
        withStyleName(MaterialTheme.CARD_1);
        withUndefinedSize();
    }

    /**
     * Bind all the fields for this form to the binder.
     *
     * @param sid the sid
     * @param clazz the clazz
     * @param coach the coach
     * @param status the status
     */
    private void setupBinder(MTextField sid, MTextField clazz, MTextField coach,
        MTextField status) {
        binder = new Binder<>();
        binder.forField(sid).bind(StudentDTO::getSid, null);
        binder.forField(clazz).bind(StudentDTO::getClazz, null);
        binder.forField(coach).bind(s -> CoachDTO.getFormattedName(s.getCoach()), null);
        binder.forField(status).bind(StudentDTO::getStatus, null);
    }

    /**
     * Set the student for this form.
     *
     * @param student the student
     */
    public void setStudent(StudentDTO student) {
        binder.readBean(student);
    }
}
