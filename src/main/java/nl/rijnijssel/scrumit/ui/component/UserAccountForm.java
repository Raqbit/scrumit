/*
 * Scrumit
 * Copyright (C) 2019  Bram Ceulemans (bramceulemans@me.com)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

package nl.rijnijssel.scrumit.ui.component;

import com.github.appreciated.material.MaterialTheme;
import com.vaadin.data.Binder;
import com.vaadin.data.HasValue.ValueChangeListener;
import com.vaadin.spring.annotation.ViewScope;
import com.vaadin.ui.Label;
import com.vaadin.ui.TextField;
import java.util.Locale;
import javax.annotation.PostConstruct;
import nl.rijnijssel.scrumit.application.I18n;
import nl.rijnijssel.scrumit.config.ScrumitConfiguration;
import nl.rijnijssel.scrumit.domain.dto.UserDTO;
import nl.rijnijssel.scrumit.ui.container.component.ColoredLabel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.vaadin.viritin.components.LocaleSelect;
import org.vaadin.viritin.layouts.MFormLayout;

@Component
@ViewScope
public class UserAccountForm extends MFormLayout {

    private final ScrumitConfiguration configuration;

    private Binder<UserDTO> binder = new Binder<>();
    private LocaleSelect language;

    @Autowired
    public UserAccountForm(ScrumitConfiguration configuration) {
        this.configuration = configuration;
    }

    @PostConstruct
    private void postConstruct() {
        /* Create all other needed fields */
        Label header = new ColoredLabel(I18n.get("account.user.header"));
        TextField name = new TextField(I18n.get("account.user.name"));
        TextField email = new TextField(I18n.get("account.user.email"));
        /* Setup the language combobox */
        language = new LocaleSelect();
        language.setPlaceholder(I18n.get("account.user.language"));
        language.setCaption(I18n.get("account.user.language"));
        /* Get all supported locales and add them to the combobox */
        language.setItems(I18n.getSupportedLocales());
        language.setTextInputAllowed(false);
        language.setEmptySelectionAllowed(false);
        /* Bind the fields to the binder */
        setupBinder(name, email, language);
        /* Add all fields to the layout */
        with(header, name, email, language);
        withStyleName(MaterialTheme.CARD_1);
        withUndefinedSize();
    }

    /**
     * Set up the field bindings.
     *
     * @param name the name field
     * @param email the email field
     * @param language the language field
     */
    private void setupBinder(TextField name, TextField email, LocaleSelect language) {
        binder.forField(name).bind(UserDTO::getName, null);
        binder.forField(email).bind(UserDTO::getEmail, null);
        binder.forField(language)
            .withNullRepresentation(configuration.getDefaultLocale())
            .bind(UserDTO::getLanguage, UserDTO::setLanguage);
    }

    /**
     * Set the user for this viewer.
     *
     * @param user the user
     */
    public void setUser(UserDTO user) {
        binder.readBean(user);
    }

    /**
     * Add a listener to when the language field changes.
     *
     * @param listener the value change listener
     */
    public void addLanguageChangeListener(ValueChangeListener<Locale> listener) {
        language.addValueChangeListener(listener);
    }
}
