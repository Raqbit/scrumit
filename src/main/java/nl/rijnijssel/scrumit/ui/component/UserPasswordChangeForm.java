/*
 * Scrumit
 * Copyright (C) 2019  Bram Ceulemans (bramceulemans@me.com)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

package nl.rijnijssel.scrumit.ui.component;

import com.github.appreciated.material.MaterialTheme;
import com.vaadin.server.UserError;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.PasswordField;
import java.util.function.Consumer;
import javax.annotation.PostConstruct;
import nl.rijnijssel.scrumit.application.I18n;
import nl.rijnijssel.scrumit.application.annotation.PrototypeScope;
import nl.rijnijssel.scrumit.domain.dto.UserDTO;
import nl.rijnijssel.scrumit.ui.container.SNotification;
import nl.rijnijssel.scrumit.ui.container.component.ColoredLabel;
import nl.rijnijssel.scrumit.web.security.SecurityUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.vaadin.viritin.button.PrimaryButton;
import org.vaadin.viritin.layouts.MFormLayout;
import org.vaadin.viritin.layouts.MHorizontalLayout;

@Component
@PrototypeScope
public class UserPasswordChangeForm extends MFormLayout {

    private final PasswordEncoder encoder;

    private PrimaryButton confirm;
    private PasswordField newPassword1;
    private PasswordField newPassword2;
    private PasswordField old;

    @Autowired
    public UserPasswordChangeForm(PasswordEncoder encoder) {
        this.encoder = encoder;
    }

    @PostConstruct
    private void postConstruct() {
        old = new PasswordField(I18n.get("account.change_password.old"));
        old.setPlaceholder(I18n.get("account.change_password.old.caption"));
        newPassword1 = new PasswordField();
        newPassword1.setPlaceholder(I18n.get("account.change_password.new.caption1"));
        newPassword2 = new PasswordField();
        newPassword2.setPlaceholder(I18n.get("account.change_password.new.caption2"));
        HorizontalLayout changedLayout = new MHorizontalLayout(newPassword1, newPassword2)
            .withCaption(I18n.get("account.change_password.new"));
        this.confirm = new PrimaryButton(I18n.get("account.change_password.save"));
        Label header = new ColoredLabel(I18n.get("account.change_password.header"));
        with(header, old, changedLayout, confirm);
        withStyleName(MaterialTheme.CARD_1);
    }

    /**
     * Add a listener for password confirm button click. Only gets called when all the fields are
     * valid.
     *
     * @param newPasswordHandler the password change listener
     */
    public void addChangeHandler(Consumer<String> newPasswordHandler) {
        confirm.addClickListener(event -> {
            if (old.isEmpty()) {
                old.setComponentError(
                    new UserError(I18n.get("notification.account.password_empty")));
            }
            UserDTO user = SecurityUtils.getCurrentUser();
            if (!encoder.matches(old.getValue(), user != null ? user.getPassword() : null)) {
                old.setComponentError(
                    new UserError(I18n.get("notification.account.password_incorrect")));
                SNotification
                    .warning(I18n.error(), I18n.get("notification.account.password_incorrect"));
            }
            if (newPassword1.isEmpty()) {
                newPassword1.setComponentError(
                    new UserError(I18n.get("notification.account.password_blank")));
            }
            if (newPassword2.isEmpty()) {
                newPassword2.setComponentError(
                    new UserError(I18n.get("notification.account.password_blank")));
            }
            String p1 = newPassword1.getValue();
            String p2 = newPassword2.getValue();
            if (!StringUtils.equals(p1, p2)) {
                SNotification
                    .warning(I18n.error(), I18n.get("notification.account.passwords_not_equal"));
                return;
            }
            String password = new String(p1.toCharArray().clone());
            newPasswordHandler.accept(password);
        });
    }
}
