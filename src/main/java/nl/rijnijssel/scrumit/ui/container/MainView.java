/*
 * Scrumit
 * Copyright (C) 2019  Bram Ceulemans (bramceulemans@me.com)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

package nl.rijnijssel.scrumit.ui.container;

import com.google.common.collect.Maps;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewDisplay;
import com.vaadin.server.Resource;
import com.vaadin.spring.access.SecuredViewAccessControl;
import com.vaadin.spring.annotation.SpringViewDisplay;
import java.util.HashMap;
import java.util.Map;
import javax.annotation.PostConstruct;
import nl.rijnijssel.scrumit.application.I18n;
import nl.rijnijssel.scrumit.domain.dto.UserDTO;
import nl.rijnijssel.scrumit.ui.ScrumitNavigator;
import nl.rijnijssel.scrumit.ui.ScrumitUI;
import nl.rijnijssel.scrumit.ui.view.AboutView;
import nl.rijnijssel.scrumit.ui.view.AccountView;
import nl.rijnijssel.scrumit.ui.view.AnswerView;
import nl.rijnijssel.scrumit.ui.view.CoachView;
import nl.rijnijssel.scrumit.ui.view.DashboardView;
import nl.rijnijssel.scrumit.ui.view.SettingView;
import nl.rijnijssel.scrumit.ui.view.StudentView;
import nl.rijnijssel.scrumit.web.security.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.vaadin.teemusa.sidemenu.SideMenu.MenuClickHandler;
import org.vaadin.teemusa.sidemenu.SideMenu.MenuRegistration;

@SpringViewDisplay
public class MainView extends MainViewDesign implements ViewDisplay {

    private final Map<Class<? extends View>, String> navigationButtons = new HashMap<>();
    private final Map<Class<? extends View>, MenuRegistration> registration = Maps.newHashMap();
    private final ScrumitNavigator navigator;
    private final SecuredViewAccessControl accessControl;

    /**
     * MainView constructor.
     */
    @Autowired
    public MainView(ScrumitNavigator navigator, SecuredViewAccessControl accessControl) {
        this.navigator = navigator;
        this.accessControl = accessControl;
    }

    /**
     * Initialize the MainView. At this point all dependencies are injected.
     */
    @PostConstruct
    public void init() {
        setLocale(I18n.getCurrentLocale());
        buildNavigation();
    }

    /**
     * Build the navigation bar, including buttons.
     */
    private void buildNavigation() {
        /* Set the user menu information */
        UserDTO currentUser = SecurityUtils.getCurrentUser();
        if (currentUser != null) {
            String name = currentUser.getName();
            name = name.substring(0, name.indexOf(" "));
            menu.setUserName(name);
            menu.setUserIcon(VaadinIcons.USER);
        }

        /* Add all menu items */
        addUserMenuItem("view.account", VaadinIcons.USER_CARD, AccountView.class);
        addUserMenuItem("view.about", VaadinIcons.INFO, AboutView.class);

        addSignOutItem(this::logout);
        addMenuItem("view.dashboard", VaadinIcons.DASHBOARD, DashboardView.class);
        addMenuItem("view.students", VaadinIcons.USERS, StudentView.class);
        addMenuItem("view.coaches", VaadinIcons.SPECIALIST, CoachView.class);
        addMenuItem("view.answers", VaadinIcons.PENCIL, AnswerView.class);
        addMenuItem("view.settings", VaadinIcons.COG, SettingView.class);
    }

    /**
     * Add a entry to the user menu.
     */
    private void addUserMenuItem(String messageKey, Resource icon,
        Class<? extends View> targetView) {
        boolean hasAccessToView = accessControl.isAccessGranted(targetView);

        if (hasAccessToView) {
            String text = I18n.get(messageKey, getLocale());
            MenuClickHandler clickHandler = () -> navigator.navigateTo(targetView);
            menu.addUserMenuItem(text, icon, clickHandler);
            navigationButtons.put(targetView, messageKey);
        }
    }

    /**
     * Add a entry to the user menu.
     *
     * @param clickHandler callback for when the button is clicked
     */
    private void addSignOutItem(final MenuClickHandler clickHandler) {
        final String text = I18n.get("app.logout", getLocale());
        menu.addUserMenuItem(text, VaadinIcons.SIGN_OUT, clickHandler);
    }

    /**
     * Adds a click listener to the given button if current user has permission to that view. If the
     * current user does not have permission, the button is hidden.
     *
     * @param messageKey name of the button
     * @param icon icon for the button
     * @param targetView view the button should go to
     */
    private void addMenuItem(String messageKey, Resource icon,
        Class<? extends View> targetView) {
        boolean hasAccessToView = accessControl.isAccessGranted(targetView);

        if (hasAccessToView) {
            String text = I18n.get(messageKey, getLocale());
            MenuClickHandler clickHandler = () -> navigator.navigateTo(targetView);
            MenuRegistration menuRegistration = menu.addMenuItem(text, icon, clickHandler);
            navigationButtons.put(targetView, messageKey);
            registration.put(targetView, menuRegistration);
        }
    }

    /**
     * Show a view, add the view components.
     *
     * @param view view to be shown
     */
    @Override
    public void showView(View view) {
        content.setContent(view.getViewComponent());

        Class<? extends View> viewClass = view.getClass();
        String viewName = navigationButtons.get(viewClass);
        if (viewName != null) {
            /* Update the page title */
            String pageTitle = I18n.get(viewName, getLocale());
            String appTitle = I18n.get("app.name");
            getUI().getPage().setTitle(pageTitle + " - " + appTitle);
        }
    }

    /**
     * Logs the user out.
     */
    private void logout() {
        ScrumitUI ui = (ScrumitUI) getUI();
        ui.logout();
    }
}
