/*
 * Scrumit
 * Copyright (C) 2018  Bram Ceulemans (bramceulemans@me.com)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

package nl.rijnijssel.scrumit.ui.container;

import com.github.appreciated.material.MaterialTheme;
import com.vaadin.shared.Position;
import javax.annotation.Nullable;
import org.vaadin.viritin.ui.MNotification;

public class SNotification {

    private SNotification() {
        // Should not be constructed
    }

    /**
     * Show a error notification in the top right.
     *
     * @param message message for the notication, nullable
     * @return the notification object
     */
    public static MNotification error(String message) {
        return error(message, null);
    }

    /**
     * Show a error notification in the top right.
     *
     * @param message message for the notication, nullable
     * @param description description for the notification, nullable
     * @return the notification object
     */
    public static MNotification error(String message, String description) {
        return MNotification.error(message, description)
            .withPosition(Position.TOP_RIGHT);
    }

    /**
     * Show a warning notification in the top right.
     *
     * @param message message for the notification, nullable
     * @return the notification object
     */
    public static MNotification warning(String message) {
        return warning(message, null);
    }

    /**
     * Show a warning notification in the top right.
     *
     * @param message message for the notification, nullable
     * @param description description for the notification, nullable
     * @return the notification object
     */
    public static MNotification warning(String message, String description) {
        return MNotification.warning(message, description)
            .withPosition(Position.TOP_RIGHT);
    }

    /**
     * Show a success notification in the top right.
     *
     * @param message message for the notication, nullable
     * @return the notification object
     */
    public static MNotification success(@Nullable String message) {
        return success(message, null);
    }

    /**
     * Show a success notification in the top right.
     *
     * @param message message for the notication, nullable
     * @param description description for the notification, nullable
     * @return the notification object
     */
    public static MNotification success(@Nullable String message, @Nullable String description) {
        return MNotification.tray(message, description)
            .withStyleName(MaterialTheme.NOTIFICATION_SUCCESS)
            .withPosition(Position.TOP_RIGHT);
    }

    /**
     * Display an information message.
     *
     * @param message message for the notication, nullable
     * @param description description for the notification, nullable
     * @return the notification object
     */
    public static MNotification info(String message, String description) {
        return MNotification.tray(message, description)
            .withStyleName(MaterialTheme.NOTIFICATION_TRAY)
            .withPosition(Position.TOP_RIGHT);
    }
}
