/*
 * Scrumit
 * Copyright (C) 2018  Bram Ceulemans (bramceulemans@me.com)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

package nl.rijnijssel.scrumit.ui.container.component;

import com.vaadin.data.HasValue.ValueChangeListener;
import org.vaadin.viritin.layouts.MHorizontalLayout;

/**
 * Header view that can be used for most views.
 */
public class HeaderLayout extends MHorizontalLayout {

    private final TitleLabel titleLabel;
    private SearchField searchField;
    private final boolean hasSearchField;

    /**
     * Constructor for HeaderLayout.
     *
     * @param title the title for this header layout
     */
    public HeaderLayout(String title) {
        this(title, true);
    }

    /**
     * Constructor for HeaderLayout.
     *
     * @param title the title for this header layout
     * @param hasSearchField if this header layout has a search field.
     */
    public HeaderLayout(String title, boolean hasSearchField) {
        titleLabel = new TitleLabel(title);
        expand(titleLabel);

        this.hasSearchField = hasSearchField;
        if (hasSearchField) {
            searchField = new SearchField();
            add(searchField);
        }
    }

    public TitleLabel getTitleLabel() {
        return titleLabel;
    }

    /**
     * Get the search field, if enabled. Otherwise return null
     *
     * @return the search field or null if it search field isn't enabled
     */
    public SearchField getSearchField() {
        if (hasSearchField) {
            return searchField;
        }

        return null;
    }

    /**
     * Get the value of the search field.
     *
     * @return the search field's value
     */
    public String getSearchValue() {
        if (hasSearchField) {
            return searchField.getValue();
        }
        return null;
    }

    /**
     * Add a search value change listener for the search field.
     *
     * @param valueChangeListener the value change listener
     */
    public void addSearchListener(ValueChangeListener<String> valueChangeListener) {
        if (searchField == null) {
            return;
        }
        searchField.addValueChangeListener(valueChangeListener);
    }
}
