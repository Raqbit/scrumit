/*
 * Scrumit
 * Copyright (C) 2019  Bram Ceulemans (bramceulemans@me.com)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

package nl.rijnijssel.scrumit.ui.container.component;

import static com.github.appreciated.material.MaterialTheme.TEXTFIELD_FLOATING;
import static com.github.appreciated.material.MaterialTheme.TEXTFIELD_INLINE_ICON;

import com.vaadin.icons.VaadinIcons;
import nl.rijnijssel.scrumit.application.I18n;
import org.vaadin.viritin.fields.MTextField;

/**
 * Scrumit's search field component, implemented in most views that contain a grid.
 */
public class SearchField extends MTextField {

    /**
     * Constructor for the search field component.
     */
    public SearchField() {
        withPlaceholder(I18n.get("component.search_caption"))
            .withIcon(VaadinIcons.SEARCH)
            .withStyleName(TEXTFIELD_INLINE_ICON, TEXTFIELD_FLOATING)
            .withAutocompleteOff()
            .withAutoCapitalizeOff();
    }
}
