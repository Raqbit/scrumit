/*
 * Scrumit
 * Copyright (C) 2019  Bram Ceulemans (bramceulemans@me.com)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

package nl.rijnijssel.scrumit.ui.presenter;

import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.spring.annotation.VaadinSessionScope;
import lombok.extern.slf4j.Slf4j;
import nl.rijnijssel.scrumit.application.utils.AppInfoUtils;

@SpringComponent
@VaadinSessionScope
@Slf4j
public class AboutPresenter {

    /**
     * Get the current implementation version from the manifest.
     *
     * @return the implementation version ({@link java.util.jar.Attributes.Name#IMPLEMENTATION_VERSION}
     */
    public String getVersion() {
        return AppInfoUtils.getVersion();
    }
}
