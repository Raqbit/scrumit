/*
 * Scrumit
 * Copyright (C) 2019  Bram Ceulemans (bramceulemans@me.com)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

package nl.rijnijssel.scrumit.ui.presenter;

import com.vaadin.data.HasValue.ValueChangeEvent;
import com.vaadin.spring.annotation.VaadinSessionScope;
import java.util.Locale;
import java.util.function.Consumer;
import nl.rijnijssel.scrumit.application.annotation.RolePattern;
import nl.rijnijssel.scrumit.domain.Role;
import nl.rijnijssel.scrumit.domain.dto.CoachDTO;
import nl.rijnijssel.scrumit.domain.dto.StudentDTO;
import nl.rijnijssel.scrumit.domain.dto.UserDTO;
import nl.rijnijssel.scrumit.domain.service.CoachService;
import nl.rijnijssel.scrumit.domain.service.StudentService;
import nl.rijnijssel.scrumit.domain.service.UserService;
import nl.rijnijssel.scrumit.ui.ScrumitUI;
import nl.rijnijssel.scrumit.ui.component.CoachAccountForm;
import nl.rijnijssel.scrumit.ui.component.StudentAccountForm;
import nl.rijnijssel.scrumit.ui.component.UserAccountForm;
import nl.rijnijssel.scrumit.web.security.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

@Component
@VaadinSessionScope
public class AccountPresenter {

    private final UserService userService;
    private final PasswordEncoder encoder;
    private final CoachService coachService;
    private final StudentService studentService;

    @Autowired
    public AccountPresenter(UserService userService, PasswordEncoder encoder,
        CoachService coachService, StudentService studentService) {
        this.userService = userService;
        this.encoder = encoder;
        this.coachService = coachService;
        this.studentService = studentService;
    }

    /**
     * Executed whenever the user clicks "change password".
     */
    public void handlePasswordChange(String password) {
        UserDTO user = userService.findByEmail(SecurityUtils.getEmail());
        changeUserPassword(user, password);
        ScrumitUI.getCurrent().logout("pwchanged");
    }


    /**
     * Changes the password for {@link SecurityUtils#getCurrentUser()}.
     *
     * @param user user to set the password for
     * @param password the password to set it to
     */
    private void changeUserPassword(final UserDTO user, final String password) {
        if (user == null) {
            return;
        }
        final String encodedPassword = encoder.encode(password);
        user.setPassword(encodedPassword);
        userService.save(user);
    }


    /**
     * Handle the language change event. Only change the language
     *
     * @param event the language change event
     */
    public void handleLanguageChange(ValueChangeEvent<Locale> event) {
        /* Only reload the page if the user triggered a value change */
        if (event.isUserOriginated()) {
            Locale locale = event.getValue();
            /* Update locale for current user */
            UserDTO current = SecurityUtils.getCurrentUser();
            if (current == null) {
                return;
            }
            current.setLanguage(locale);

            /* Update the user in the database */
            UserDTO user = userService.get(current.getId());
            user.setLanguage(locale);
            userService.save(user);

            /* Reload the page so localization changes apply */
            ScrumitUI.getCurrent().getPage().reload();
        }
    }

    /**
     * Set up the form visibility for AccountView.
     *
     * @param userForm the user form
     * @param studentForm the student form
     */
    public void handleFormVisibility(UserAccountForm userForm, StudentAccountForm studentForm,
        CoachAccountForm coachForm) {
        UserDTO user = SecurityUtils.getCurrentUser();
        if (user == null) {
            return;
        }
        @RolePattern String role = user.getRole();
        switch (role) {
            case Role.ADMIN:
                userForm.setVisible(true);
                coachForm.setVisible(false);
                studentForm.setVisible(false);
                break;
            case Role.COACH:
                userForm.setVisible(true);
                coachForm.setVisible(true);
                studentForm.setVisible(false);
                break;
            case Role.STUDENT:
                userForm.setVisible(true);
                coachForm.setVisible(false);
                studentForm.setVisible(true);
                break;
            default:
                break;
        }
    }

    /**
     * Set the callbacks for what should happen to all the form content.
     *
     * @param userConsumer the consumer for the user found
     * @param studentConsumer the consumer for the student found, if the current user is a student
     * @param coachConsumer the consumer for the coach found, if the current user is a coach
     */
    public void setAccountFormContent(Consumer<UserDTO> userConsumer,
        Consumer<StudentDTO> studentConsumer, Consumer<CoachDTO> coachConsumer) {
        String email = SecurityUtils.getEmail();
        if (email == null) {
            return;
        }
        UserDTO user = userService.findByEmail(email);
        if (user == null) {
            return;
        }
        userConsumer.accept(user);
        if (SecurityUtils.isCurrentUserInRole(Role.COACH)) {
            CoachDTO coach = coachService.findByUser(user);
            coachConsumer.accept(coach);
        }
        if (SecurityUtils.isCurrentUserInRole(Role.STUDENT)) {
            StudentDTO student = studentService.findByUser(user);
            studentConsumer.accept(student);
        }
    }
}
