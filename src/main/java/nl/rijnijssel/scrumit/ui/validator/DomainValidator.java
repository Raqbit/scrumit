/*
 * Scrumit
 * Copyright (C) 2018  Bram Ceulemans (bramceulemans@me.com)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

package nl.rijnijssel.scrumit.ui.validator;

import com.vaadin.data.validator.RegexpValidator;

/**
 * Validator for Vaadin Binders, checks if a string is a valid domain.
 */
public class DomainValidator extends RegexpValidator {

    private static final String domainRegexp =
        "^[a-zA-Z0-9][a-zA-Z0-9-]{1,61}[a-zA-Z0-9]\\.[a-zA-Z]{2,}$";

    /**
     * DomainValidator constructor.
     *
     * @param errorMessage error message to give when the test fails
     */
    public DomainValidator(String errorMessage) {
        super(errorMessage, domainRegexp);
    }
}
