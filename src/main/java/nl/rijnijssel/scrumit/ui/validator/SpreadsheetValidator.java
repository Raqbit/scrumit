/*
 * Scrumit
 * Copyright (C) 2018  Bram Ceulemans (bramceulemans@me.com)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

package nl.rijnijssel.scrumit.ui.validator;

import com.vaadin.data.ValidationResult;
import com.vaadin.data.ValueContext;
import com.vaadin.data.validator.AbstractValidator;
import nl.rijnijssel.scrumit.application.integration.sheets.SheetsIntegration;

public class SpreadsheetValidator extends AbstractValidator<String> {

    private final SheetsIntegration sheetsIntegration;

    /**
     * Constructs a validator with the given error message. The substring "{0}" is replaced by the
     * value that failed validation.
     *
     * @param errorMessage the message to be included in a failed result, not null
     */
    public SpreadsheetValidator(String errorMessage, SheetsIntegration sheetsIntegration) {
        super(errorMessage);
        this.sheetsIntegration = sheetsIntegration;
    }

    @Override
    public ValidationResult apply(String value, ValueContext context) {
        return toResult(value, isValid(value));
    }

    private boolean isValid(String value) {
        return sheetsIntegration.sheetExists(value);
    }
}
