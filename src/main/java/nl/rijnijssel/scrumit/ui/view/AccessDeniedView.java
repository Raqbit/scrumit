/*
 * Scrumit
 * Copyright (C) 2019  Bram Ceulemans (bramceulemans@me.com)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

package nl.rijnijssel.scrumit.ui.view;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.spring.annotation.SpringView;
import javax.annotation.PostConstruct;
import lombok.extern.slf4j.Slf4j;
import nl.rijnijssel.scrumit.domain.dto.UserDTO;
import nl.rijnijssel.scrumit.ui.container.component.TitleLabel;
import nl.rijnijssel.scrumit.web.security.SecurityUtils;
import org.springframework.stereotype.Component;
import org.vaadin.viritin.layouts.MVerticalLayout;

@Component("scrumit-access-denied-view")
@SpringView
@Slf4j
public class AccessDeniedView extends MVerticalLayout implements View {

    @PostConstruct
    private void postConstruct() {
        TitleLabel accessDeniedLabel = new TitleLabel("Access denied");
        with(accessDeniedLabel).withMargin(false);
    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event) {
        // Redirect to the default view
        UserDTO currentUser = SecurityUtils.getCurrentUser();
        View oldView = event.getOldView();
        String role = "";
        String email = "";
        if (currentUser != null) {
            role = currentUser.getRole();
            email = currentUser.getEmail();
        }
        log.debug("Access denied in view {} for {} {}", oldView, role, email);
    }
}
