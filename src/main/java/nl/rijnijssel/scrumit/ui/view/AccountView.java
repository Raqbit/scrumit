/*
 * Scrumit
 * Copyright (C) 2019  Bram Ceulemans (bramceulemans@me.com)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

package nl.rijnijssel.scrumit.ui.view;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.spring.annotation.SpringView;
import javax.annotation.PostConstruct;
import nl.rijnijssel.scrumit.application.I18n;
import nl.rijnijssel.scrumit.domain.Role;
import nl.rijnijssel.scrumit.ui.component.CoachAccountForm;
import nl.rijnijssel.scrumit.ui.component.StudentAccountForm;
import nl.rijnijssel.scrumit.ui.component.UserAccountForm;
import nl.rijnijssel.scrumit.ui.component.UserPasswordChangeForm;
import nl.rijnijssel.scrumit.ui.container.component.HeaderLayout;
import nl.rijnijssel.scrumit.ui.presenter.AccountPresenter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.vaadin.viritin.layouts.MFormLayout;

@SpringView(name = "account")
@Secured({Role.STUDENT, Role.COACH, Role.ADMIN})
public class AccountView extends MFormLayout implements View {

    private final UserAccountForm userAccountForm;
    private final StudentAccountForm studentAccountForm;
    private final CoachAccountForm coachAccountForm;
    private final UserPasswordChangeForm userPasswordChangeForm;
    private final AccountPresenter presenter;

    /**
     * Dependency injection constructor for ProfileView.
     */
    @Autowired
    public AccountView(UserPasswordChangeForm userPasswordChangeForm,
        UserAccountForm userAccountForm, AccountPresenter presenter,
        StudentAccountForm studentAccountForm, CoachAccountForm coachAccountForm) {
        this.userPasswordChangeForm = userPasswordChangeForm;
        this.userAccountForm = userAccountForm;
        this.presenter = presenter;
        this.studentAccountForm = studentAccountForm;
        this.coachAccountForm = coachAccountForm;
    }

    @PostConstruct
    private void init() {
        HeaderLayout header = new HeaderLayout(I18n.get("account.title"), false);
        with(header, userAccountForm, coachAccountForm, studentAccountForm, userPasswordChangeForm);
        userAccountForm.addLanguageChangeListener(presenter::handleLanguageChange);
        userPasswordChangeForm.addChangeHandler(presenter::handlePasswordChange);
    }


    @Override
    public void enter(ViewChangeEvent event) {
        presenter.handleFormVisibility(userAccountForm, studentAccountForm, coachAccountForm);
        presenter.setAccountFormContent(userAccountForm::setUser, studentAccountForm::setStudent,
            coachAccountForm::setCoach);
    }

}
