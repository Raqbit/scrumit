/*
 * Scrumit
 * Copyright (C) 2019  Bram Ceulemans (bramceulemans@me.com)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

package nl.rijnijssel.scrumit.ui.view;

import com.vaadin.data.HasValue;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.Grid;
import com.vaadin.ui.Window;
import java.util.List;
import javax.annotation.PostConstruct;
import nl.rijnijssel.scrumit.application.I18n;
import nl.rijnijssel.scrumit.domain.Role;
import nl.rijnijssel.scrumit.domain.dto.AnswerDTO;
import nl.rijnijssel.scrumit.domain.dto.CoachDTO;
import nl.rijnijssel.scrumit.domain.dto.StudentDTO;
import nl.rijnijssel.scrumit.domain.dto.UserDTO;
import nl.rijnijssel.scrumit.domain.service.AnswerService;
import nl.rijnijssel.scrumit.domain.service.CoachService;
import nl.rijnijssel.scrumit.domain.service.StudentService;
import nl.rijnijssel.scrumit.ui.component.AnswerCreateForm;
import nl.rijnijssel.scrumit.ui.component.Grids;
import nl.rijnijssel.scrumit.ui.container.SNotification;
import nl.rijnijssel.scrumit.ui.container.component.HeaderLayout;
import nl.rijnijssel.scrumit.ui.container.component.IconButton;
import nl.rijnijssel.scrumit.web.security.SecurityUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.env.Environment;
import org.springframework.core.env.Profiles;
import org.springframework.security.access.annotation.Secured;
import org.vaadin.viritin.layouts.MVerticalLayout;


/**
 * View where students can see their answers and make a new scrum answer.
 */
@SpringView(name = "answers")
@Secured({Role.STUDENT})
public class AnswerView extends MVerticalLayout implements View {

    private final AnswerService answerService;
    private final CoachService coachService;
    private final StudentService studentService;
    private final Environment environment;
    private final Grid<AnswerDTO> grid;

    private HeaderLayout header;
    private IconButton createAnswer;
    private AnswerCreateForm form;

    private StudentDTO currentStudent;
    private UserDTO currentUser;

    /**
     * AnswerView constructor.
     *
     * @param answerService the answer service
     * @param coachService the coach service
     * @param studentService the student service
     */
    @Autowired
    public AnswerView(AnswerService answerService, CoachService coachService,
        StudentService studentService, Environment environment,
        @Qualifier(Grids.ANSWER) Grid<AnswerDTO> grid) {
        this.answerService = answerService;
        this.coachService = coachService;
        this.studentService = studentService;
        this.environment = environment;
        this.grid = grid;
    }

    /**
     * Initialize this view.
     */
    @PostConstruct
    public void init() {
        this.currentUser = SecurityUtils.getCurrentUser();
        this.currentStudent = studentService.findByUser(currentUser);


        /* Answer popup window */
        form = new AnswerCreateForm();

        header = new HeaderLayout(I18n.get("answers.title"));
        createAnswer = new IconButton(VaadinIcons.PENCIL);
        header.add(createAnswer);

        /* Set the components and styling for this view */
        add(header, grid);
        buildAnswerForm();
    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event) {
        header.addSearchListener(this::search);
        /* Add click listener to the new answer button */
        createAnswer.addClickListener(click -> openAnswerWindow());
        form.setResetHandler(answer -> form.closePopup());
        reset();
    }

    private void buildAnswerForm() {
        /* Listeners for the Answer window */
        form.setEntity(new AnswerDTO(currentStudent));
        form.setSavedHandler(this::saveAnswer);
    }

    /**
     * Save a new answer for a user.
     *
     * @param answer the answer to save
     */
    private void saveAnswer(AnswerDTO answer) {
        answerService.save(answer);
        form.setEntity(new AnswerDTO(currentStudent));
        SNotification.success(I18n.get("notification.answer.submitted"));
        form.closePopup();
        reset();
    }

    private void reset() {
        /* Refresh answer grid */
        List<AnswerDTO> answers = answerService.findByOwner(currentStudent);
        grid.setItems(answers);

        /* Add all available coaches in the combo box */
        List<CoachDTO> coaches = coachService.getAll();
        form.coach.setItems(coaches);
    }


    /**
     * Search with a query, returns all answers for this user when empty.
     *
     * @param event value change event
     */
    private void search(HasValue.ValueChangeEvent<String> event) {
        String search = StringUtils.stripToNull(event.getValue());
        List<AnswerDTO> filteredAnswers = answerService
            .findAnyMatchingForUser(search, currentUser);
        grid.setItems(filteredAnswers);
    }


    /**
     * Open the answer window or show an error if the student has already answered today.
     */
    private void openAnswerWindow() {
        boolean isProduction = environment.acceptsProfiles(Profiles.of("production"));
        boolean answeredToday = answerService.hasAnsweredToday(currentStudent);
        if (isProduction && answeredToday) {
            SNotification.warning(I18n.get("notification.answer.already_answered"));
        } else {
            Window window = form.openInModalPopup();
            window.setResizable(false);
            window.setDraggable(false);
            window.center();
        }
    }
}
