/*
 * Scrumit
 * Copyright (C) 2018  Bram Ceulemans (bramceulemans@me.com)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

package nl.rijnijssel.scrumit.ui.view;

import com.vaadin.data.HasValue.ValueChangeEvent;
import com.vaadin.event.selection.SelectionEvent;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.Grid;
import java.util.List;
import java.util.Optional;
import javax.annotation.PostConstruct;
import lombok.extern.slf4j.Slf4j;
import nl.rijnijssel.scrumit.application.I18n;
import nl.rijnijssel.scrumit.domain.Role;
import nl.rijnijssel.scrumit.domain.dto.CoachDTO;
import nl.rijnijssel.scrumit.domain.dto.UserDTO;
import nl.rijnijssel.scrumit.domain.service.CoachService;
import nl.rijnijssel.scrumit.domain.service.SettingService;
import nl.rijnijssel.scrumit.domain.service.UserService;
import nl.rijnijssel.scrumit.ui.component.CoachEditForm;
import nl.rijnijssel.scrumit.ui.container.SNotification;
import nl.rijnijssel.scrumit.ui.container.component.HeaderLayout;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.vaadin.viritin.layouts.MVerticalLayout;

@SpringView(name = "coaches")
@Secured(Role.ADMIN)
@Slf4j
public class CoachView extends MVerticalLayout implements View {

    private final CoachService coachService;
    private final UserService userService;
    private final Grid<CoachDTO> grid;
    private final SettingService settingService;
    private HeaderLayout header;
    private CoachEditForm editor;


    /**
     * CoachView constructor for dependency injection.
     *
     * @param coachService the coach service
     */
    @Autowired
    public CoachView(CoachService coachService, UserService userService, Grid<CoachDTO> grid,
        SettingService settingService) {
        this.coachService = coachService;
        this.userService = userService;
        this.grid = grid;
        this.settingService = settingService;
    }

    /**
     * Initialize the CoachView.
     */
    @PostConstruct
    private void init() {
        header = new HeaderLayout(I18n.get("coaches.title"));
        editor = new CoachEditForm(settingService);
        add(header, grid);
    }

    /**
     * Called each time the search function is used.
     *
     * @param event the value change event
     */
    private void search(ValueChangeEvent<String> event) {
        String search = StringUtils.stripToNull(event.getValue());
        List<CoachDTO> coaches = coachService.findAnyMatching(search);
        grid.setItems(coaches);
    }

    /**
     * Gets executed everytime you enter this view.
     *
     * @param event view change event
     */
    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event) {
        /* Add a value change listener to the search field, this performs the search */
        header.addSearchListener(this::search);

        /* Add a selection listener to the grid, opens the coach edit layout */
        grid.addSelectionListener(this::onCoachSelected);

        /* Add a click listener to the save button inside edit coach layout, performs a save */
        editor.setSavedHandler(this::saveCoach);
        reload();
    }

    /**
     * Reset the view.
     */
    private void reload() {
        List<CoachDTO> coaches = coachService.getAll();
        grid.setItems(coaches);
    }

    /**
     * Update the coach editor panel.
     *
     * @param event selection event
     */
    private void onCoachSelected(SelectionEvent<CoachDTO> event) {
        if (event.isUserOriginated()) {
            Optional<CoachDTO> coachDTO = event.getFirstSelectedItem();
            if (coachDTO.isPresent()) {
                /* Called when a coach is selected */
                editor.openInModalPopup();
                String abbreviation = coachDTO.get().getAbbreviation();
                CoachDTO coach = coachService.findByAbbreviation(abbreviation);
                if (coach.getUser() == null) {
                    UserDTO user = new UserDTO();
                    user.setRole(Role.COACH);
                    coach.setUser(user);
                }
                editor.setEntity(coach);
            } else {
                /* Called when a coach is deselected */
                editor.setEntity(null);
            }
        }
    }

    /**
     * Save a coach and it's user (if present).
     *
     * @param coach the coach to save
     */
    private void saveCoach(CoachDTO coach) {
        UserDTO user = coach.getUser();
        if (user != null) {
            String password = coachService.generateDefaultPassword();
            user.setPassword(password);
        }

        if (!userService.canAuthenticate(user)) {
            coachService.save(coach);
            SNotification.warning(I18n.get("notification.coach.saved"),
                I18n.get("notification.coach.no_authentication"));
            return;
        }

        if (user != null) {
            /* Create the new coach, with a password and role set to Coach */
            configureCoach(coach, user);

            /* Save the coach to the database and refresh the coach grid (reload) */
            coach = coachService.save(coach);
            reload();

            SNotification.success(I18n.get("notification.coach.saved"));
            log.debug("Updated coach: {}", coach);
        }
        editor.closePopup();
    }


    /**
     * Create new coach, and fill in the user password / role if the coach has no user.
     *
     * @param coach coach to create
     * @param user user to save with the coach
     */
    private void configureCoach(CoachDTO coach, UserDTO user) {
        String encodedPassword = coachService.generateDefaultPassword();
        user.setPassword(encodedPassword);
        user.setRole(Role.COACH);
        user = userService.save(user);
        coach.setUser(user);
    }
}
