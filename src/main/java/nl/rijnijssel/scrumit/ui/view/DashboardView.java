/*
 * Scrumit
 * Copyright (C) 2019  Bram Ceulemans (bramceulemans@me.com)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

package nl.rijnijssel.scrumit.ui.view;

import com.github.appreciated.material.MaterialTheme;
import com.google.common.collect.Sets;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.shared.data.sort.SortDirection;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.DateField;
import com.vaadin.ui.Grid;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import javax.annotation.PostConstruct;
import lombok.extern.slf4j.Slf4j;
import nl.rijnijssel.scrumit.application.I18n;
import nl.rijnijssel.scrumit.application.utils.DateUtils;
import nl.rijnijssel.scrumit.domain.Role;
import nl.rijnijssel.scrumit.domain.dto.AnswerDTO;
import nl.rijnijssel.scrumit.domain.service.AnswerService;
import nl.rijnijssel.scrumit.ui.component.Grids;
import nl.rijnijssel.scrumit.ui.container.component.HeaderLayout;
import nl.rijnijssel.scrumit.ui.container.component.IconButton;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.access.annotation.Secured;
import org.vaadin.addons.ComboBoxMultiselect;
import org.vaadin.viritin.layouts.MVerticalLayout;

@SpringView(name = "dashboard")
@Secured({Role.ADMIN, Role.COACH})
@Slf4j
public class DashboardView extends MVerticalLayout implements View {

    private final AnswerService answerService;
    private final Grid<AnswerDTO> grid;

    private HeaderLayout header;
    private DateField date;
    private ComboBoxMultiselect<String> table;
    private ComboBox<String> project;


    @Autowired
    public DashboardView(AnswerService answerService,
        @Qualifier(Grids.DASHBOARD) Grid<AnswerDTO> grid) {
        this.answerService = answerService;
        this.grid = grid;
    }

    /**
     * Post constructor for DashboardView.
     */
    @PostConstruct
    public void init() {
        header = new HeaderLayout(I18n.get("dashboard.title"));

        date = new DateField();
        date.addStyleName(MaterialTheme.DATEFIELD_FLOATING);
        date.setValue(LocalDate.now());
        date.setPlaceholder(I18n.get("dashboard.datepicker_caption"));
        date.setDescription("Date of which the scrums should be shown");

        table = new ComboBoxMultiselect<>();
        table.addStyleName(MaterialTheme.COMBOBOX_FLOATING);
        table.setItemIconGenerator(item -> VaadinIcons.TABLE);
        table.setItemCaptionGenerator(
            i -> String.format(I18n.get("dashboard.tablepicker_item_caption"), i));
        table.setPlaceholder(I18n.get("dashboard.tablepicker_caption"));

        project = new ComboBox<>();
        project.addStyleName(MaterialTheme.COMBOBOX_FLOATING);
        project.setPlaceholder(I18n.get("dashboard.projectpicker_caption"));
        project.setEmptySelectionCaption(I18n.get("dashboard.projectpicker_empty"));
        project.addValueChangeListener(e -> search());

        IconButton refresh = new IconButton(VaadinIcons.REFRESH);
        header.add(date, project, table, refresh);
        header.getSearchField().setDescription("Search all scrums");

        /* Date field listener */
        date.addValueChangeListener(e -> {
            search();
            updateTableList();
        });
        /* Table field listener */
        table.addValueChangeListener(e -> search());
        /* Search field listener */
        header.addSearchListener(e -> {
            updateTableList();
            search();
        });
        /* Refresh button listener */
        refresh.addClickListener(e -> search());

        add(header, grid);
    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event) {
        reset();
    }

    /**
     * Reset the view, update the answers in the grid.
     */
    private void reset() {
        updateTableList();
        updateProjectList();
        search();
    }

    private void updateTableList() {
        List<String> tables = answerService.getTables();
        Set<String> selectedItems = table.getSelectedItems();
        table.setItems(tables);
        table.select(selectedItems.toArray(new String[]{}));
    }

    private void updateProjectList() {
        List<String> projects = answerService.getProjects();
        Optional<String> selectedItem = project.getSelectedItem();
        project.setItems(projects);
        selectedItem.ifPresent(a -> project.setSelectedItem(a));
    }

    /**
     * Search through all answers.
     */
    private void search() {
        String search = StringUtils.stripToNull(header.getSearchValue());
        Date dateFieldValue;
        if (date.isEmpty()) {
            dateFieldValue = new Date();
        } else {
            dateFieldValue = DateUtils.toDate(date.getValue());
        }
        /* Filter according to date field and search field */
        List<AnswerDTO> queriesAnswers = new ArrayList<>();
        for (AnswerDTO answer : answerService.findAnyMatching(search)) {
            if (DateUtils.isSameDay(answer.getCreatedAt(), dateFieldValue)) {
                queriesAnswers.add(answer);
            }
        }
        queriesAnswers = answerService.filterAnswersByTables(queriesAnswers, table.getValue());
        Set<String> projectSet = Sets.newHashSet(project.getValue());
        if (!project.isEmpty()) {
            queriesAnswers = answerService.filterAnswersByProjects(queriesAnswers, projectSet);
        }
        /* Set the items to the grid */
        grid.setItems(queriesAnswers);
        grid.sort("created", SortDirection.DESCENDING);
    }
}
