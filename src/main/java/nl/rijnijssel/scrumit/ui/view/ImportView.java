/*
 * Scrumit
 * Copyright (C) 2018  Bram Ceulemans (bramceulemans@me.com)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

package nl.rijnijssel.scrumit.ui.view;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.spring.annotation.SpringView;
import javax.annotation.PostConstruct;
import nl.rijnijssel.scrumit.domain.Role;
import nl.rijnijssel.scrumit.ui.container.component.HeaderLayout;
import org.springframework.security.access.annotation.Secured;
import org.vaadin.viritin.layouts.MVerticalLayout;

@SpringView(name = "importer")
@Secured({Role.ADMIN})
public class ImportView extends MVerticalLayout implements View {

    public ImportView() {
        // Empty constructor
    }

    @PostConstruct
    private void init() {
        add(new HeaderLayout("Import"));
    }

    @Override
    public void enter(ViewChangeEvent event) {

    }
}
