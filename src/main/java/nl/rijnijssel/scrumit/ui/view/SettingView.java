/*
 * Scrumit
 * Copyright (C) 2018  Bram Ceulemans (bramceulemans@me.com)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

package nl.rijnijssel.scrumit.ui.view;

import com.github.appreciated.material.MaterialTheme;
import com.vaadin.data.Binder;
import com.vaadin.data.ValidationException;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewBeforeLeaveEvent;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.spring.annotation.SpringView;
import javax.annotation.PostConstruct;
import lombok.extern.slf4j.Slf4j;
import nl.rijnijssel.scrumit.application.I18n;
import nl.rijnijssel.scrumit.application.integration.sheets.SheetsIntegration;
import nl.rijnijssel.scrumit.application.utils.StudentUtils;
import nl.rijnijssel.scrumit.domain.Role;
import nl.rijnijssel.scrumit.domain.SettingName;
import nl.rijnijssel.scrumit.domain.dto.SettingDTO;
import nl.rijnijssel.scrumit.domain.service.SettingService;
import nl.rijnijssel.scrumit.ui.container.SNotification;
import nl.rijnijssel.scrumit.ui.container.component.HeaderLayout;
import nl.rijnijssel.scrumit.ui.validator.DomainValidator;
import nl.rijnijssel.scrumit.ui.validator.SpreadsheetValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.security.access.annotation.Secured;
import org.vaadin.dialogs.ConfirmDialog;
import org.vaadin.viritin.button.MButton;
import org.vaadin.viritin.button.PrimaryButton;
import org.vaadin.viritin.fields.MCheckBox;
import org.vaadin.viritin.fields.MTextField;
import org.vaadin.viritin.label.MLabel;
import org.vaadin.viritin.layouts.MFormLayout;
import org.vaadin.viritin.layouts.MHorizontalLayout;
import org.vaadin.viritin.layouts.MVerticalLayout;

@SpringView(name = "admin-settings")
@Secured(Role.ADMIN)
@Slf4j
public class SettingView extends MVerticalLayout implements View {

    private final SettingService settingService;
    private final SheetsIntegration sheetsIntegration;
    private final Binder<SettingDTO> spreadsheetEnabledBind;
    private final Binder<SettingDTO> spreadsheetIdBind;
    private final Binder<SettingDTO> defaultPasswordBind;
    private final Binder<SettingDTO> defaultEmailDomainBind;
    private SettingDTO defaultPasswordSetting;
    private SettingDTO spreadsheetIdSetting;
    private SettingDTO spreadsheetEnabledSetting;
    private SettingDTO defaultEmailDomainSetting;
    private boolean settingsChanged = false;

    private MCheckBox enabled;
    private MButton sync;
    private MTextField spreadsheetId;

    private MTextField defaultPassword;

    private PrimaryButton save;
    private MButton reset;
    private MTextField defaultEmailDomain;

    /**
     * SettingView constructor for dependency injection.
     *
     * @param settingService the setting service
     * @param sheetsIntegration the sheets integration
     */
    @Autowired
    public SettingView(SettingService settingService, SheetsIntegration sheetsIntegration) {
        this.settingService = settingService;
        this.sheetsIntegration = sheetsIntegration;

        /* Initialize fields */
        // FIXME: 9-12-17 use a hashmap with setting enum and binder key value pairs to list \
        // FIXME: 9-12-17 all settings and create them dynamically
        this.spreadsheetEnabledBind = new Binder<>();
        this.spreadsheetIdBind = new Binder<>();
        this.defaultPasswordBind = new Binder<>();
        this.defaultEmailDomainBind = new Binder<>();
    }


    /**
     * Initialize the setting view.
     */
    @PostConstruct
    private void init() {

        enabled = new MCheckBox(I18n.get("settings.gsheets.enabled"));
        sync = new MButton(I18n.get("settings.gsheets.synchronize"))
            .withStyleName(MaterialTheme.BUTTON_PRIMARY);
        spreadsheetId = new MTextField(I18n.get("settings.gsheets.spreadsheet_id"))
            .withFullSize();

        defaultPassword = new MTextField(I18n.get("settings.student.default_password"))
            .withFullSize();
        defaultEmailDomain = new MTextField(I18n.get("settings.student.default_email"))
            .withFullSize();

        save = new PrimaryButton(I18n.get("settings.save"));
        reset = new MButton(I18n.get("settings.discard"))
            .withStyleName(MaterialTheme.BUTTON_QUIET);
        MHorizontalLayout bottomBar = new MHorizontalLayout().add(save, reset);
        MLabel sheetsTitle = new MLabel(I18n.get("settings.gsheets.header"))
            .withStyleName(MaterialTheme.LABEL_H3, MaterialTheme.LABEL_COLORED);
        MHorizontalLayout enabledLayout = new MHorizontalLayout()
            .add(enabled, sync)
            .withCaption(I18n.get("settings.gsheets.synchronization"));
        MLabel studentsTitle = new MLabel("Students")
            .withStyleName(MaterialTheme.LABEL_H3, MaterialTheme.LABEL_COLORED);
        MFormLayout formLayout = new MFormLayout(sheetsTitle, enabledLayout, spreadsheetId,
            studentsTitle,
            defaultPassword, defaultEmailDomain, bottomBar)
            .withMargin(false);

        HeaderLayout headerLayout = new HeaderLayout(I18n.get("settings.title"), false);
        add(headerLayout, formLayout);
    }

    /**
     * Executed everytime you enter the view.
     *
     * @param event view change event
     */
    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event) {
        spreadsheetEnabledBind
            .bind(enabled, SettingDTO::getBooleanValue, SettingDTO::setBooleanValue);
        spreadsheetIdBind.forField(spreadsheetId)
            .withValidator(
                new SpreadsheetValidator(I18n.get("settings.gsheets.sheet_not_found"),
                    sheetsIntegration))
            .bind(SettingDTO::getStringValue, SettingDTO::setStringValue);
        defaultPasswordBind.forField(defaultPassword)
            .bind(SettingDTO::getStringValue, SettingDTO::setStringValue);
        defaultEmailDomainBind.forField(defaultEmailDomain)
            .withValidator(new DomainValidator("This is not a valid domain"))
            .bind(SettingDTO::getStringValue, SettingDTO::setStringValue);
        spreadsheetEnabledBind.addValueChangeListener(e -> valueChange());
        spreadsheetIdBind.addValueChangeListener(e -> valueChange());
        defaultPasswordBind.addValueChangeListener(e -> valueChange());
        defaultEmailDomainBind.addValueChangeListener(e -> valueChange());
        sync.addClickListener(e -> synchronize());
        save.addClickListener(e -> save());
        reset.addClickListener(e -> reset());
        reset();
    }

    @Override
    public void beforeLeave(ViewBeforeLeaveEvent event) {
        if (settingsChanged) {
            String confirmText = "Settings are not saved, do you want to discard your changes?";

            ConfirmDialog confirmDialog = ConfirmDialog.show(getUI(), confirmText, dialog -> {
                if (dialog.isConfirmed()) {
                    event.navigate();
                }
            });

            confirmDialog.getCancelButton().addStyleName(MaterialTheme.BUTTON_PRIMARY);
            confirmDialog.getCancelButton().setCaption("Stay");
            confirmDialog.getOkButton().setStyleName(MaterialTheme.BUTTON_DANGER);
            confirmDialog.getOkButton().setCaption("Discard");
        } else {
            event.navigate();
        }
    }

    /**
     * Save all the settings.
     */
    private void save() {
        try {
            /* Write the beans to their fields */
            writeBeans();

            /* Save the beans to the database */
            settingService.save(spreadsheetEnabledSetting);
            settingService.save(spreadsheetIdSetting);
            settingService.save(defaultPasswordSetting);
            settingService.save(defaultEmailDomainSetting);
            SNotification.success("Successfully saved settings.");

            /* Reset the view so the beans are read from the database */
            reset();
        } catch (ValidationException e) {
            SNotification.error("Settings could not be saved.");
        }
    }

    /**
     * Write the bindings to their fields.
     *
     * @throws ValidationException when the fields fail validation
     */
    private void writeBeans() throws ValidationException {
        spreadsheetEnabledBind.writeBean(spreadsheetEnabledSetting);
        spreadsheetIdBind.writeBean(spreadsheetIdSetting);
        defaultPasswordBind.writeBean(defaultPasswordSetting);
        defaultEmailDomainBind.writeBean(defaultEmailDomainSetting);
    }

    /**
     * Reset the view to it's default.
     */
    private void reset() {
        spreadsheetEnabledSetting = settingService.getSetting(SettingName.SPREADSHEETS_ENABLED);
        spreadsheetIdSetting = settingService.getSetting(SettingName.SPREADSHEET_ID);
        defaultPasswordSetting = settingService.getSetting(SettingName.STUDENT_DEFAULT_PASSWORD);
        defaultEmailDomainSetting = settingService
            .getSetting(SettingName.STUDENT_DEFAULT_EMAIL_DOMAIN);

        spreadsheetEnabledBind.readBean(spreadsheetEnabledSetting);
        spreadsheetIdBind.readBean(spreadsheetIdSetting);
        defaultPasswordBind.readBean(defaultPasswordSetting);
        defaultEmailDomainBind.readBean(defaultEmailDomainSetting);
        settingsChanged = false;
    }

    /**
     * Synchronize the students with the given sheet.
     */
    @Async
    public void synchronize() {
        getUI().access(() -> {
            int size = StudentUtils.synchronize(sheetsIntegration, spreadsheetId.getValue()).size();
            SNotification
                .success("Synchronization done", String.format("%s students imported", size));
        });
    }

    private void valueChange() {
        log.debug("Settings have changed.");

        settingsChanged = spreadsheetEnabledBind.hasChanges() || spreadsheetIdBind.hasChanges()
            || defaultPasswordBind.hasChanges() || defaultEmailDomainBind.hasChanges();
    }
}
