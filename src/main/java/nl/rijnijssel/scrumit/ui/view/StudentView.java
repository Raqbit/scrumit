/*
 * Scrumit
 * Copyright (C) 2018  Bram Ceulemans (bramceulemans@me.com)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

package nl.rijnijssel.scrumit.ui.view;

import com.vaadin.data.HasValue.ValueChangeEvent;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.Grid;
import java.util.List;
import javax.annotation.PostConstruct;
import nl.rijnijssel.scrumit.application.I18n;
import nl.rijnijssel.scrumit.domain.Role;
import nl.rijnijssel.scrumit.domain.dto.StudentDTO;
import nl.rijnijssel.scrumit.domain.service.StudentService;
import nl.rijnijssel.scrumit.ui.container.component.HeaderLayout;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.vaadin.viritin.layouts.MVerticalLayout;

/**
 * View displaying all registered students, including a search field.
 *
 * @see StudentService
 * @see StudentDTO
 */
@SpringView(name = "students")
@Secured({Role.ADMIN, Role.COACH})
public class StudentView extends MVerticalLayout implements View {

    private final StudentService studentService;
    private final Grid<StudentDTO> grid;
    private HeaderLayout header;


    /**
     * Dependency injection constructor for StudentView.
     *
     * @param studentService the student service
     */
    @Autowired
    public StudentView(StudentService studentService, Grid<StudentDTO> grid) {
        this.studentService = studentService;
        this.grid = grid;
    }

    /**
     * Post constructor for StudentView.
     */
    @PostConstruct
    public void init() {
        header = new HeaderLayout(I18n.get("students.title"));
        add(header, grid);
    }

    /**
     * Called everytime this view is displayed.
     *
     * @param event the view change event
     */
    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event) {
        /* Add listener to search bar at the top */
        header.addSearchListener(this::search);

        /* Load students into Grid */
        List<StudentDTO> studentList = studentService.getAll();
        grid.setItems(studentList);
    }

    /**
     * Called whenever the search field value changes.
     *
     * @param event the value change event
     */
    private void search(ValueChangeEvent<String> event) {
        String search = StringUtils.stripToNull(event.getValue());
        List<StudentDTO> students = studentService.findAnyMatching(search);
        grid.setItems(students);
    }
}
