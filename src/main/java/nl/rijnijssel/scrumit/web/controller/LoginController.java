/*
 * Scrumit
 * Copyright (C) 2019  Bram Ceulemans (bramceulemans@me.com)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

package nl.rijnijssel.scrumit.web.controller;

import static nl.rijnijssel.scrumit.application.ScrumitConstants.APP_URL;
import static nl.rijnijssel.scrumit.application.ScrumitConstants.LOGIN_URL;

import nl.rijnijssel.scrumit.web.security.SecurityUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class LoginController implements ErrorController {

    @Value("${spring.application.name}")
    String appName;

    @GetMapping(path = "/")
    public String redirectToApp() {
        return "redirect:app";
    }

    /**
     * Show the login view.
     *
     * @param model model for the attributes in thymeleaf
     * @return the log in view
     */
    @GetMapping(path = LOGIN_URL)
    public String displayLoginView(Model model) {
        if (SecurityUtils.getPrincipal() != null) {
            return "redirect:app";
        }
        model.addAttribute("name", appName);
        model.addAttribute("description", "Rijn IJssel's scrum application");
        return "sign-in";
    }

    @Override
    public String getErrorPath() {
        return APP_URL;
    }
}
