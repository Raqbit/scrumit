/*
 * Scrumit
 * Copyright (C) 2019  Bram Ceulemans (bramceulemans@me.com)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

package nl.rijnijssel.scrumit.web.security;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import javax.annotation.Nullable;
import nl.rijnijssel.scrumit.domain.dto.UserDTO;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;

/**
 * SecurityUtils takes care of most static authentication checking.
 */
public class SecurityUtils {

    private SecurityUtils() {
        // Static utility methods only
    }

    /**
     * Get the current ScrumitPrincipal.
     *
     * @return the current principal
     */
    @Nullable
    public static ScrumitPrincipal getPrincipal() {
        SecurityContext context = SecurityContextHolder.getContext();
        if (context == null || context.getAuthentication() == null) {
            return null;
        }
        Object principal = context.getAuthentication().getPrincipal();
        if (principal instanceof String) {
            return null;
        }
        return (ScrumitPrincipal) principal;
    }

    /**
     * Get the current authenticated user.
     *
     * @return current authenticated user
     */
    @Nullable
    public static UserDTO getCurrentUser() {
        ScrumitPrincipal principal = getPrincipal();
        if (principal == null) {
            return null;
        }
        return principal.getUser();
    }

    /**
     * Gets the email of the user that's currently logged in.
     *
     * @return email of the current principal
     */
    @Nullable
    public static String getEmail() {
        UserDTO currentUser = getCurrentUser();
        if (currentUser == null) {
            return null;
        }
        return currentUser.getEmail();
    }

    /**
     * Check if the current user is in a role.
     *
     * @param role role to check against
     * @return if the user is in the given role
     */
    public static boolean isCurrentUserInRole(String role) {
        List<String> userRoles = getUserRoles();
        return userRoles != null && userRoles.stream()
            .filter(Objects::nonNull)
            .anyMatch(roleName -> roleName.equals(Objects.requireNonNull(role)));
    }

    /**
     * Get a list of all user roles.
     *
     * @return list of all user roles
     */
    private static List<String> getUserRoles() {
        ScrumitPrincipal principal = getPrincipal();
        if (principal == null) {
            return null;
        }
        return principal.getAuthorities()
            .stream()
            .map(GrantedAuthority::getAuthority)
            .collect(Collectors.toList());
    }
}
