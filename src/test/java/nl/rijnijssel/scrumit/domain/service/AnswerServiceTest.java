/*
 * Scrumit
 * Copyright (C) 2018  Bram Ceulemans (bramceulemans@me.com)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

package nl.rijnijssel.scrumit.domain.service;

import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;

import java.util.List;
import java.util.Optional;
import nl.rijnijssel.scrumit.config.ScrumitBeans;
import nl.rijnijssel.scrumit.config.ScrumitConfiguration;
import nl.rijnijssel.scrumit.domain.Role;
import nl.rijnijssel.scrumit.domain.dto.AnswerDTO;
import nl.rijnijssel.scrumit.domain.dto.StudentDTO;
import nl.rijnijssel.scrumit.domain.dto.UserDTO;
import nl.rijnijssel.scrumit.domain.repository.AnswerRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FilterType;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@DataJpaTest(includeFilters = @ComponentScan.Filter(
    classes = {
        AnswerService.class,
        StudentService.class,
        UserService.class,
        ScrumitConfiguration.class,
        ScrumitBeans.class
    },
    type = FilterType.ASSIGNABLE_TYPE
))
public class AnswerServiceTest {

    @Autowired
    private AnswerService answerService;
    @Autowired
    private StudentService studentService;
    @Autowired
    private UserService userService;

    private StudentDTO student1;
    private StudentDTO student2;
    private AnswerDTO answer1;
    private AnswerDTO answer2;
    private AnswerDTO answer3;
    private AnswerDTO answer4;

    /**
     * Generate test answers.
     */
    @Before
    public void setUp() {
        UserDTO userStudent1 = new UserDTO("user1@example.com", "User 1", "secret",
            Role.STUDENT);
        userStudent1 = userService.save(userStudent1);
        student1 = new StudentDTO("00891423", userStudent1, "Status 1", "Class A", 1, null);
        student1 = studentService.save(student1);

        UserDTO userStudent2 = new UserDTO("user2@example.com", "User 2", "secret",
            Role.STUDENT);
        userStudent2 = userService.save(userStudent2);
        student2 = new StudentDTO("00234789", userStudent2, "Status 2", "Class B", 2, null);
        student2 = studentService.save(student2);

        answer1 = new AnswerDTO("I used Node.js.", "Node.js had a problem.",
            "Debugging Node.js.",
            "No need for help.", null, student1);
        answer1 = answerService.save(answer1);
        answer2 = new AnswerDTO("I used PHP.", "PHP had a problem.", "Debugging PHP.",
            "Need help with PHP.", null, student1);
        answer2 = answerService.save(answer2);
        answer3 = new AnswerDTO("I used Java.", "Java had a problem.", "Debugging Java.",
            "No need for help.", null, student2);
        answer3 = answerService.save(answer3);
        answer4 = new AnswerDTO("I used Haskell.", "Haskell is hard.", "Debugging Haskell.",
            "Need help with Haskell.", null, student2);
        answer4 = answerService.save(answer4);
    }

    @Test
    public void getRepository() {
        assertThat(answerService.getRepository(), notNullValue());
        assertThat(answerService.getRepository(), instanceOf(AnswerRepository.class));
    }

    @Test
    public void findByOwner() {
        Optional<AnswerDTO> foundAnswer = answerService.findByOwner(student1).stream()
            .findFirst();
        assertThat(foundAnswer.isPresent(), is(true));
        // TODO: 11-12-17 fix this testcase - assertThat(foundAnswer.orElseGet(null), is(answer2));

        List<AnswerDTO> foundAnswers = answerService.findByOwner(student2);
        assertThat(foundAnswers, contains(answer4, answer3));
        assertThat(foundAnswers, not(contains(answer2, answer1)));
    }

    @Test
    public void hasAnsweredToday() {
        boolean answered = answerService.hasAnsweredToday(student1);
        assertThat(answered, is(true));

        answered = answerService.hasAnsweredToday(student2);
        assertThat(answered, is(true));
    }

    @Test
    public void findAnyMatchingForUser() {
        List<AnswerDTO> foundAnswers = answerService
            .findAnyMatchingForUser("PHP", student1.getUser());
        assertThat(foundAnswers, hasSize(1));
        assertThat(foundAnswers, contains(answer2));

        foundAnswers = answerService.findAnyMatchingForUser("Java", student2.getUser());
        assertThat(foundAnswers, hasSize(1));
        assertThat(foundAnswers, contains(answer3));

        foundAnswers = answerService.findAnyMatchingForUser("Debugging", student1.getUser());
        assertThat(foundAnswers, hasSize(2));
        assertThat(foundAnswers, contains(answer2, answer1));

        foundAnswers = answerService.findAnyMatchingForUser("Debugging", student2.getUser());
        assertThat(foundAnswers, hasSize(2));
        assertThat(foundAnswers, contains(answer4, answer3));
    }

    @Test
    public void countAnyMatching() {
        long foundAnswers = answerService.countAnyMatching("php");
        assertThat(foundAnswers, is(1L));

        foundAnswers = answerService.countAnyMatching("java");
        assertThat(foundAnswers, is(1L));

        foundAnswers = answerService.countAnyMatching("debugging");
        assertThat(foundAnswers, is(4L));

        foundAnswers = answerService.countAnyMatching(null);
        assertThat(foundAnswers, is(4L));

        foundAnswers = answerService.countAnyMatching("need help with");
        assertThat(foundAnswers, is(2L));
    }

    @Test
    public void findAnyMatching() {
        List<AnswerDTO> foundAnswers = answerService.findAnyMatching("php");
        assertThat(foundAnswers, hasSize(1));
        assertThat(foundAnswers, contains(answer2));

        foundAnswers = answerService.findAnyMatching("java");
        assertThat(foundAnswers, hasSize(1));
        assertThat(foundAnswers, contains(answer3));

        foundAnswers = answerService.findAnyMatching("debugging");
        assertThat(foundAnswers, hasSize(4));
        assertThat(foundAnswers, contains(answer4, answer3, answer2, answer1));

        foundAnswers = answerService.findAnyMatching(null);
        assertThat(foundAnswers, hasSize(4));
        assertThat(foundAnswers, containsInAnyOrder(answer4, answer3, answer2, answer1));

        foundAnswers = answerService.findAnyMatching("need help with");
        assertThat(foundAnswers, hasSize(2));
        assertThat(foundAnswers, contains(answer4, answer2));
    }

}