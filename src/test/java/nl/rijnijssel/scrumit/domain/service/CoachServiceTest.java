/*
 * Scrumit
 * Copyright (C) 2018  Bram Ceulemans (bramceulemans@me.com)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

package nl.rijnijssel.scrumit.domain.service;

import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;

import nl.rijnijssel.scrumit.config.ScrumitBeans;
import nl.rijnijssel.scrumit.config.ScrumitConfiguration;
import nl.rijnijssel.scrumit.domain.Role;
import nl.rijnijssel.scrumit.domain.dto.CoachDTO;
import nl.rijnijssel.scrumit.domain.dto.UserDTO;
import nl.rijnijssel.scrumit.domain.repository.CoachRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FilterType;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@DataJpaTest(includeFilters = @ComponentScan.Filter(
    classes = {
        CoachService.class,
        SettingService.class,
        UserService.class,
        ScrumitConfiguration.class,
        ScrumitBeans.class
    },
    type = FilterType.ASSIGNABLE_TYPE
))
public class CoachServiceTest {

    @Autowired
    private CoachService coachService;
    @Autowired
    private UserService userService;
    private CoachDTO coach1;
    private CoachDTO coach2;

    /**
     * Generate test coaches.
     */
    @Before
    public void setUp() {
        UserDTO userCoach1 = new UserDTO("user1@example.com", "User 1", "secret",
            Role.STUDENT);
        userCoach1 = userService.save(userCoach1);

        coach1 = new CoachDTO("U1", userCoach1);
        coachService.save(coach1);

        coach2 = new CoachDTO("U2", null);
        coachService.save(coach2);
    }

    @Test
    public void getRepository() {
        assertThat(coachService.getRepository(), notNullValue());
        assertThat(coachService.getRepository(), instanceOf(CoachRepository.class));
    }

    @Test
    public void hasUser() {
        boolean hasUser = coachService.hasUser(coach1);
        assertThat(hasUser, is(true));

        hasUser = coachService.hasUser(coach2);
        assertThat(hasUser, is(false));
    }

    @Test
    public void countAnyMatching() {
        long coachCount = coachService.countAnyMatching("u1");
        assertThat(coachCount, is(1L));

        coachCount = coachService.countAnyMatching("u");
        assertThat(coachCount, is(2L));
    }

    @Test
    public void findAnyMatching() {
    }

}