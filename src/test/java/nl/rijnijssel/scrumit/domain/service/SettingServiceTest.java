/*
 * Scrumit
 * Copyright (C) 2018  Bram Ceulemans (bramceulemans@me.com)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

package nl.rijnijssel.scrumit.domain.service;

import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;

import lombok.extern.slf4j.Slf4j;
import nl.rijnijssel.scrumit.config.ScrumitBeans;
import nl.rijnijssel.scrumit.config.ScrumitConfiguration;
import nl.rijnijssel.scrumit.domain.repository.SettingRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FilterType;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@DataJpaTest(includeFilters = @ComponentScan.Filter(
    classes = {
        SettingService.class,
        ScrumitConfiguration.class,
        ScrumitBeans.class
    },
    type = FilterType.ASSIGNABLE_TYPE
))
@Slf4j
public class SettingServiceTest {

    @Autowired
    private SettingService settingService;

    @Before
    public void setUp() {
        settingService.createMissingSettings();
    }

    @Test
    public void getRepository() {
        assertThat(settingService.getRepository(), notNullValue());
        assertThat(settingService.getRepository(), instanceOf(SettingRepository.class));
    }

    @Test
    public void getStringSetting() {
    }

    @Test
    public void getBooleanSetting() {
    }

    @Test
    public void getSetting() {
    }

    @Test
    public void countAnyMatching() {
    }

    @Test
    public void findAnyMatching() {
    }

}