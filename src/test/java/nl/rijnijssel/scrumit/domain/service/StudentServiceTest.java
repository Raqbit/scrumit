/*
 * Scrumit
 * Copyright (C) 2018  Bram Ceulemans (bramceulemans@me.com)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

package nl.rijnijssel.scrumit.domain.service;

import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;

import java.util.List;
import nl.rijnijssel.scrumit.config.ScrumitBeans;
import nl.rijnijssel.scrumit.config.ScrumitConfiguration;
import nl.rijnijssel.scrumit.domain.Role;
import nl.rijnijssel.scrumit.domain.dto.StudentDTO;
import nl.rijnijssel.scrumit.domain.dto.UserDTO;
import nl.rijnijssel.scrumit.domain.repository.StudentRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FilterType;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@DataJpaTest(includeFilters = @ComponentScan.Filter(
    classes = {
        StudentService.class,
        UserService.class,
        ScrumitConfiguration.class,
        ScrumitBeans.class
    },
    type = FilterType.ASSIGNABLE_TYPE
))
public class StudentServiceTest {

    @Autowired
    private StudentService studentService;
    @Autowired
    private UserService userService;

    private StudentDTO student1;
    private StudentDTO student2;

    /**
     * Generate test students.
     */
    @Before
    public void setUp() {
        UserDTO userStudent1 = new UserDTO("user1@example.com", "User 1", "secret",
            Role.STUDENT);
        userStudent1 = userService.save(userStudent1);
        StudentDTO student1 = new StudentDTO("00891423", userStudent1, "Status 1", "Class A",
            1, null);
        this.student1 = studentService.save(student1);

        UserDTO userStudent2 = new UserDTO("user2@example.com", "User 2", "secret",
            Role.STUDENT);
        userStudent2 = userService.save(userStudent2);
        StudentDTO student2 = new StudentDTO("00234789", userStudent2, "Status 2", "Class B",
            2, null);
        this.student2 = studentService.save(student2);
    }

    @Test
    public void getRepository() {
        assertThat(studentService.getRepository(), notNullValue());
        assertThat(studentService.getRepository(), instanceOf(StudentRepository.class));
    }

    @Test
    public void countAnyMatching() {
        long studentCount = studentService.countAnyMatching("00234789");
        assertThat(studentCount, is(1L));

        studentCount = studentService.countAnyMatching("user 1");
        assertThat(studentCount, is(1L));

        studentCount = studentService.countAnyMatching("00891423");
        assertThat(studentCount, is(1L));

        studentCount = studentService.countAnyMatching("class b");
        assertThat(studentCount, is(1L));

        studentCount = studentService.countAnyMatching("class");
        assertThat(studentCount, is(2L));

        studentCount = studentService.countAnyMatching(null);
        assertThat(studentCount, is(2L));
    }

    @Test
    public void findAnyMatching() {
        List<StudentDTO> students = studentService.findAnyMatching("00234789");
        assertThat(students, hasSize(1));
        assertThat(students, contains(student2));

        students = studentService.findAnyMatching("user 1");
        assertThat(students, hasSize(1));
        assertThat(students, contains(student1));

        students = studentService.findAnyMatching("class b");
        assertThat(students.size(), is(1));
        assertThat(students, contains(student2));

        students = studentService.findAnyMatching("class");
        assertThat(students, hasSize(2));

        students = studentService.findAnyMatching(null);
        assertThat(students, hasSize(2));
    }

}