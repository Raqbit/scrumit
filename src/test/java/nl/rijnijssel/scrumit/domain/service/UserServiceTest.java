/*
 * Scrumit
 * Copyright (C) 2018  Bram Ceulemans (bramceulemans@me.com)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

package nl.rijnijssel.scrumit.domain.service;

import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;

import java.util.List;
import nl.rijnijssel.scrumit.config.ScrumitBeans;
import nl.rijnijssel.scrumit.config.ScrumitConfiguration;
import nl.rijnijssel.scrumit.domain.Role;
import nl.rijnijssel.scrumit.domain.dto.UserDTO;
import nl.rijnijssel.scrumit.domain.repository.UserRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FilterType;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@DataJpaTest(includeFilters = @ComponentScan.Filter(
    classes = {
        UserService.class,
        ScrumitConfiguration.class,
        ScrumitBeans.class
    },
    type = FilterType.ASSIGNABLE_TYPE
))
public class UserServiceTest {

    @Autowired
    private UserService userService;

    private UserDTO user1;
    private UserDTO user2Locked;

    /**
     * Generate test users.
     */
    @Before
    public void setUp() {
        user1 = new UserDTO("user1@example.com", "User 1", "secret", Role.STUDENT);
        user1.setLocked(false);
        user1 = userService.save(user1);
        user2Locked = new UserDTO("user2-locked@example.com", "User 2 (Locked)", "secret",
            Role.STUDENT);
        user2Locked.setLocked(true);
        user2Locked = userService.save(user2Locked);
    }

    @Test
    public void getRepository() {
        assertThat(userService.getRepository(), notNullValue());
        assertThat(userService.getRepository(), instanceOf(UserRepository.class));
    }

    @Test
    public void save() {
        try {
            UserDTO byEmail = userService.findByEmail("user1@example.com");
            byEmail.setName("User 1 Modified");
            byEmail = userService.save(byEmail);
            assertThat(byEmail.getName(), is("User 1 Modified"));
        } catch (UnsupportedOperationException e) {
            fail();
        }

        try {
            user2Locked.setName("User 2 Modified");
            userService.save(user2Locked);
            fail();
        } catch (UnsupportedOperationException e) {
            assertThat(e.getMessage(),
                is("User has been locked and cannot be modified or deleted"));
        }
    }

    @Test
    public void delete() {
        try {
            userService.delete(user1.getId());
            user1 = userService.findByEmail("user1@example.com");
            assertThat(user1, nullValue());
        } catch (UnsupportedOperationException e) {
            fail();
        }

        try {
            userService.delete(user2Locked.getId());
            fail();
        } catch (UnsupportedOperationException e) {
            user2Locked = userService.findByEmail("user2-locked@example.com");
            assertThat(user2Locked, notNullValue());
            assertThat(e.getMessage(),
                is("User has been locked and cannot be modified or deleted"));
        }
    }

    @Test
    public void countAnyMatching() {
        long userCount = userService.countAnyMatching("User 1");
        assertThat(userCount, is(1L));

        userCount = userService.countAnyMatching("user");
        assertThat(userCount, is(2L));

        userCount = userService.countAnyMatching("example.com");
        assertThat(userCount, is(2L));

        userCount = userService.countAnyMatching(null);
        assertThat(userCount, is(2L));
    }

    @Test
    public void findAnyMatching() {
        List<UserDTO> foundUsers = userService.findAnyMatching("User 1");
        assertThat(foundUsers, hasSize(1));
        assertThat(foundUsers, contains(user1));

        foundUsers = userService.findAnyMatching("user");
        assertThat(foundUsers, hasSize(2));

        foundUsers = userService.findAnyMatching("example.com");
        assertThat(foundUsers, hasSize(2));

        foundUsers = userService.findAnyMatching(null);
        assertThat(foundUsers, hasSize(2));
    }

}