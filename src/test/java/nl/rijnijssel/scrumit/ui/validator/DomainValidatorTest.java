/*
 * Scrumit
 * Copyright (C) 2018  Bram Ceulemans (bramceulemans@me.com)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

package nl.rijnijssel.scrumit.ui.validator;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import com.vaadin.data.ValidationResult;
import com.vaadin.data.ValueContext;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
@Slf4j
public class DomainValidatorTest {

    @Test
    public void apply() {
        log.info("in testDomainValidator");

        DomainValidator validator = new DomainValidator("Invalid domain.");

        ValidationResult result = validator.apply("example.com", new ValueContext());
        assertFalse(result.isError());

        result = validator.apply("example", new ValueContext());
        assertTrue(result.isError());
        assertEquals(result.getErrorMessage(), "Invalid domain.");

        result = validator.apply("user@example.com", new ValueContext());
        assertTrue(result.isError());
        assertEquals(result.getErrorMessage(), "Invalid domain.");

        result = validator.apply("@example.com", new ValueContext());
        assertTrue(result.isError());
        assertEquals(result.getErrorMessage(), "Invalid domain.");

        result = validator.apply("example.c", new ValueContext());
        assertTrue(result.isError());
        assertEquals(result.getErrorMessage(), "Invalid domain.");
    }

}