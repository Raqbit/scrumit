/*
 * Scrumit
 * Copyright (C) 2019  Bram Ceulemans (bramceulemans@me.com)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

package nl.rijnijssel.scrumit.web.security;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;

import nl.rijnijssel.scrumit.config.ScrumitBeans;
import nl.rijnijssel.scrumit.config.ScrumitConfiguration;
import nl.rijnijssel.scrumit.domain.Role;
import nl.rijnijssel.scrumit.domain.dto.UserDTO;
import nl.rijnijssel.scrumit.domain.service.CrudService;
import nl.rijnijssel.scrumit.domain.service.UserService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FilterType;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@DataJpaTest(includeFilters = @ComponentScan.Filter(
    classes = {
        CrudService.class,
        ScrumitUserDetailsService.class,
        ScrumitConfiguration.class,
        ScrumitBeans.class
    },
    type = FilterType.ASSIGNABLE_TYPE
))
public class ScrumitUserDetailsServiceTest {

    @Autowired
    private UserService userService;
    @Autowired
    private PasswordEncoder encoder;
    @Autowired
    private ScrumitUserDetailsService userDetailsService;

    /**
     * Generate users for testing authentication.
     */
    @Before
    public void setUp() {
        UserDTO user1 = new UserDTO("user1@example.com", "User 1", encoder.encode("secret1"),
            Role.STUDENT);
        UserDTO user2 = new UserDTO("user2@example.com", "User 2", encoder.encode("secret2"),
            Role.STUDENT);
        user2.setDisabled(true);

        userService.save(user1);
        userService.save(user2);
    }

    @Test
    public void loadUserByUsername_1() {
        UserDetails userDetails;
        try {
            userDetails = userDetailsService.loadUserByUsername("user1@example.com");
            assertThat(userDetails.getUsername(), is("user1@example.com"));
            assertThat(userDetails.isEnabled(), is(true));
        } catch (AuthenticationException e) {
            fail();
        }
    }

    @Test
    public void loadUserByUsername_2() {
        UserDetails userDetails;

        try {
            userDetails = userDetailsService.loadUserByUsername("user2@example.com");
            assertThat(userDetails.getUsername(), is("user2@example.com"));
            assertThat(userDetails.isEnabled(), is(false));
        } catch (AuthenticationException e) {
            fail();
        }
    }

    @Test
    public void loadUserByUsername_3() {
        UserDetails userDetails = null;
        try {
            userDetails = userDetailsService.loadUserByUsername("user3@example.com");
            fail();
        } catch (AuthenticationException e) {
            assertThat(userDetails, nullValue());
        }
    }
}
